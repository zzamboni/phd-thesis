#!/usr/local/bin/perl -w
#
use strict;
use lib qw(
           /home/lookout/u/dzamboni/lib/perl/lib
           /home/lookout/u/dzamboni/lib/perl/lib/site_perl
           /home/lookout/u/dzamboni/lib/perl/lib/site_perl/5.6.0
           /home/lookout/u/dzamboni/lib/perl/lib/site_perl/5.6.0/sun4-solaris);
use CGI qw(:standard *table);
use URI::Find;
use File::Copy;

######################################################################
# Configuration

my $file="/home/lookout/u/dzamboni/ids_db/ids.txt";

######################################################################
# Global variables

my %db=();
my $lastadded;

# Subroutines to add attributes according to flags.
my %flagsubs=(
	      Commercial => sub { font({color=>'red'}, shift) },
	      Government => sub { font({color=>'blue'}, shift) },
	      Research   => sub { font({color=>'black'}, shift) },
	      Community   => sub { font({color=>'green'}, shift) },
	      Recheck    => sub { i(shift) },
	      );
# Flags and order in which they appear
my @flags=qw(Research Commercial Government Community Recheck);

# Map field abbreviations to full names
my %fields=(
	    das => "Data analysis structure",
	    dcs => "Data collection structure",
	    dcm => "Data collection mechanism",
	    com => "Comments",
	    flg => "Flags",
	    url => "URLs",
	    bib => "Bibliographic entries",
	   );
# Order in which fields should be displayed
my @fields=qw(das dcs dcm com flg url bib);

# Values for some of the fields.
my %fieldvals=(
	       das => [qw(Centralized Distributed)],
	       dcs => [qw(Centralized Distributed)],
	       dcm => [qw(Indirect/NetBased Indirect/HostBased
			  Direct/External Direct/Internal)],
	       flg => [@flags],
	      );

######################################################################

read_db();

print header,
  start_html('IDS information');

# Check our arguments to see what we need to do.

if (param('show_input')) {
  Delete('hide_input');
  Delete('show_input');
  Delete('inputs_hidden');
}
elsif (param('hide_input')) {
  my $view=param('view_as_list');
  Delete_all();
  param('view_as_list', $view);
  param('inputs_hidden', 1);
}
elsif (param('Clear')) {
  my $view=param('view_as_list');
  Delete_all();
  param('view_as_list', $view);
}
elsif (param('delete')) {
  my $n=param('delete');
  Delete('delete');
  remove($n);
  exit;
}
elsif (param('view')) {
  # View details of an entry.
  view(param('view'));
  exit;
}
elsif (param('entry') || param('entry2')) {
  # Edit an existing entry
  edit();
}
elsif (param('name')) {
  if (param('Next')) {
    # Simply go to the next entry, do not enter current info
    editnext();
  }
  else {
    # Enter a new (or modified) entry
    enter();
    # Rewrite database
    write_db();
    if (param('EnterNext')) {
      # Put the next (alphabetically) system in the input fields
      editnext();
    }
    else {
      # By default just empty the text fields so that new information
      # can be entered easily The selection fields are left with their
      # old values so that similar systems can be entered in sequence.
      Delete($_) foreach (qw(name com url bib entry entry2));
    }
  }
}

# Now generate an HTML page.

# Print the input section unless we are hiding it. But always
# show it when we are providing information for editing.
if (param('name') || !param('inputs_hidden')) {
  print
    h1('Enter IDS information');

  print start_form,
    "Name: ", textfield('name'), br,
    "$fields{das}: ",
  	radio_group(-name => 'das',
	      -values => $fieldvals{das}), br,
    "$fields{dcs}: ",
  	radio_group(-name => 'dcs',
	      -values => $fieldvals{dcs}), br,
    "$fields{dcm}: ",
  	checkbox_group(-name => 'dcm',
		       -values => $fieldvals{dcm}), br,
    "$fields{flg}: ",
        checkbox_group(-name => 'flg', -values => $fieldvals{flg}), br,
    "$fields{com}: ", textfield(-name => 'com', -size=>80), br,
    table({cellspacing=>0, cellpadding => 0},
	Tr({valign => "top"},
	   [td(["URLs: ", textarea(-name=>'url', -rows=>3, -columns=>80)]),
	    td(["BIBs: ", textarea(-name=>'bib', -rows=>5, -columns=>80)])])),
    table({cellspacing=>0, cellpadding => 0, width=>"100%"},
	Tr(td(submit(-name => 'Enter'),
	      submit(-name => 'EnterNext', -label => "Enter and edit next"),
	      submit(-name => 'Next', -label => "Don't enter, edit next"),
	      submit(-name => "Clear", -label => "Clear form"),
	     ),
	   td({align => 'right'}, submit(-name => "hide_input",
					 -label => "Hide input fields")))),
    hidden(-name => 'view_as_list'), # maintain state, see below
    hidden(-name => 'inputs_hidden'),
    end_form, hr;
}

print start_form,"\n";

if (param('inputs_hidden')) {
  print table({cellspacing=>0, cellpadding=>0, width => "100%"},
	      Tr(td({align => "right"},
		    submit(-name => "show_input",
			   -label => "Show input fields"))));
}

print h1('IDS information');


# Some state information
print hidden(-name => 'inputs_hidden');

print table({width => "100%"},
	    Tr(td(submit(-name => 'Modify selected'),
		  "or enter name: ", textfield('entry2')),
	       td({align => 'right'},
		  checkbox(-name => 'view_as_list',
			   -value => 'on', 
			   -label => 'View alphabetically')),
	      ));

# Print current information in the appropriate format

if (param('view_as_list')) {
  # Print in list form
  my @ids=casei_sort(keys %db);
  autoEscape(undef);
  print radio_group(-name => 'entry',
		    -value => [@ids],
		    -labels => { map { $_ => format_name($_) } @ids },
		    -default => "-",
		    -columns => 5);
  autoEscape(1);
}
else {
  # Print in default table form
  my $hfield=param('hfield')||'das';
  my $hother=($hfield eq 'das'?'dcs':'das');
  my $vfield=param('vfield')||'dcm';
  my $vother=($vfield eq 'dcm')?'dcs':'dcm';
  print start_table({border=>1});
  print TR(td({colspan=>2, rowspan=>2, valign=>'bottom'},
	      font({size=>"-1"},
		   a({href => my_url(vfield => $vother, hfield => $hfield)},
		     "View by $fields{$vother}"))),
	   th({colspan=>2}, $fields{$hfield},
	      font({size => "-1"},
		   a({href => my_url(hfield => $hother, vfield => $vfield)},
		     "View by $fields{$hother}"))));
  print TR(th([map { sprintf("$_ (%s%%)", pct_bycat($hfield => [$_]))
		   } @{$fieldvals{$hfield}} ]));
  my $prev_c="";
  my %count_dcm;
  $count_dcm{Indirect}=pct_bycat(dcm => ['Indirect/NetBased', 
					 'Indirect/HostBased']);
  $count_dcm{Direct}=pct_bycat(dcm => ['Direct/External', 'Direct/Internal']);
  foreach my $v (@{$fieldvals{$vfield}}) {
    print "<tr>";
    if ($vfield eq 'dcm') {
      # Split into class and subclass for formatting the table
      my ($c,$sc)=split("/", $v);
      if ($c ne $prev_c) {
	print th({rowspan=>"2"}, sprintf("$c%s(%s%%)", br, $count_dcm{$c}));
      }
      $prev_c=$c;
      print th(sprintf("$sc%s(%s%%)",br, pct_bycat(dcm => [$v]))),"\n";
    }
    else {
      if (!$prev_c) {
	print th({rowspan=>"2"}, join(br,split(' ',$fields{$vfield})));
      }
      $prev_c=$v;
      print th(sprintf("$v%s(%s%%)", br, pct_bycat($vfield=>[$v])));
    }
    foreach my $h (@{$fieldvals{$hfield}}) {
      autoEscape(undef);
      print td(join(", ", map { radio_group(-name => 'entry',
					    -values => [$_],
					    -labels => {$_ => format_name($_)},
					    -default => "-") } 
		    get_idss($hfield => $h, $vfield => $v))),"\n";
      autoEscape(1);
    }
    print "</tr>\n";
  }
  print end_table;
}

# Print common stuff

# Caption for the flags
print "<small>Flags: ";
print join(", ", map { $flagsubs{$_}->($_) } @flags);
print "</small>",br;

# A second submit button
print submit(-name => 'Modify selected'), p, end_form;

# Existing comments
unless (param('nocomments')) {
  print h2("Comments");
  print table({cellpadding => 0, cellspacing => 0},
	      Tr([map { td([b(format_name($_).": "), $db{$_}->{com}]) }
		  grep { $db{$_}->{com} } casei_sort(keys %db)]));
}
# Some counts
unless (param('nostats')) {
  print h2("Other stats");
  print "Host-based: ",
    pct_bycat(dcm => ['Indirect/HostBased',
		      'Direct/External',
		      'Direct/Internal']),
    "%, Network-based: ",
    pct_bycat(dcm => ['Indirect/NetBased']),"%",p;
  my @condboth=({dcm => ['Indirect/NetBased']},
		{dcm => ['Indirect/HostBased',
			 'Direct/External',
			 'Direct/Internal']});
  print "Systems that have both HB and NB: ",pct_bycat_isect(@condboth),
        "% (".join(", ",get_bycat_isect(@condboth)).")",p;
  #print "Data collection structure: ",
  #    map { sprintf("$_ (%.2f%%) ",
  #		  100*scalar(get_idss(dcs => $_))/scalar(keys %db)) }
  #@{$fieldvals{dcs}};
  print scalar(keys %db)." IDSs in total.",br;
  print join(br, map { scalar(get_idss(flg => $_)).
			 " of them $_." } @flags);
}
print end_html;

######################################################################

# Return the IDSs that satisfy the conditions given.
sub get_idss {
  my %arg=@_;
  my ($das, $dcs, $dcm, $flg)=@arg{qw(das dcs dcm flg)};
  my @res=();
  foreach my $n (casei_sort(keys %db)) {
    push @res, $n
      if ( (!$das || $das eq $db{$n}->{das}) &&
	   (!$dcs || $dcs eq $db{$n}->{dcs}) &&
	   (!$dcm || $db{$n}->{dcm} =~ /$dcm/) &&
	   (!$flg || $db{$n}->{flg} =~ /$flg/)
	 );
  }
  return @res;
}

######################################################################

# Case insensitive sort
sub casei_sort {
  return(map { $_->[1] }
	 sort { $a->[0] cmp $b->[0] }
	 map { [lc($_), $_] } @_);
}

######################################################################

# Get all the systems that belong to any of the given categories.
# Args: cat1 => [value1, value2], cat2 => [value1], etc.
sub get_bycat {
  my %arg=@_;
  my %res;
  foreach my $c (keys %arg) {
    foreach my $v (@{$arg{$c}}) {
      @res{get_idss($c => $v)} = ();
    }
  }
  return keys %res;
}

# Same as above, but return the count only
sub count_bycat {
  return scalar(get_bycat(@_));
}

# Same as the above, but return the percentage of such systems,
# formatted to two decimal places.
sub pct_bycat {
  return sprintf("%.2f", 100*count_bycat(@_)/scalar(keys %db));
}

######################################################################

# Return the systems that satisfy all sets of conditions.
# Args: {cond1 => [...],...}, {cond2 => [...],...},...
# Each condition is given as in get_bycat().
sub get_bycat_isect {
  my @args=@_;
  my %res;
  foreach my $c (@args) {
    $res{$_}++ foreach (get_bycat(%$c));
  }
  return grep { $res{$_}==scalar(@args) } keys %res;
}

sub count_bycat_isect {
  return scalar(get_bycat_isect(@_));
}

sub pct_bycat_isect {
  return sprintf("%.2f", 100*count_bycat_isect(@_)/scalar(keys %db));
}

######################################################################

# Apply modifiers and format according to the entry's flags
sub format_name {
  my $n=shift;
  my $flg=$db{$n}->{flg};
  my $res=$n;
  # Add attributes according to flags
  foreach my $f (keys %flagsubs) {
      if ($flg =~ /$f/) {
	  $res=$flagsubs{$f}->($res);
      }
  }
  # Flag the last added system in bold
  if ($lastadded && $n eq $lastadded) {
      $res=b($res);
  }
  # If there is additional information, make it a link to the
  # information screen.
  if ($db{$n}->{url}||$db{$n}->{bib}) {
    $res=a({href => my_url(view => $n)}, $res);
  }
  return $res;
}

######################################################################

# Read database in memory, store it in %db.

# The format of the file is:
# name,das,dcs,dcm,flg,com
# URLs:
# <urls>
# endURLs
# BIBs:
# <bibs>
# endBIBs
#
# Where:
#   das - Data Analysis Structure
#   dcs - Data Collection Structure
#   dcm - Data Collection Mechanisms
#   flg - Flags
#   com - Comments
# The URLs and BIBs sections are optional.
#
# We really should use XML... :-)
#
# The format of %db is:
# %db -> {name} -> {das}, {dcs}, {dcm}, {flg}, {com}, {url}, {bib}
sub read_db {
  my @lines=();			# temporary line buffer
  my $getline=sub { my $l=(shift(@lines) || <F>); $l };
  my $putback=sub { unshift @lines, @_ };

  if (open F, "<$file") {
    while ($_=&$getline) {
      chomp;
      my ($n,$das,$dcs,$dcm,$flg,$com)=split(/,/, $_, 6);
      $flg=~s/Freeware/Community/;
      next unless $n =~ /\S/;
      $db{$n}={dcs => $dcs,	# data collection structure
	       das => $das,	# data analysis structure
	       dcm => $dcm,	# data collection mechanism
	       com => $com,	# comment
	       flg => $flg||"Research", # flags
	      };
      my $tmp=&$getline;
      my $url="";
      if ($tmp && $tmp=~/^URLs:/) {
	while ($_=&$getline) {
	  last if /^endURLs/;
	  $url.=$_;
	}
      } else {
	$putback->($tmp);
      }
      $tmp=&$getline;
      my $bib="";
      if ($tmp && $tmp=~/^BIBs:/) {
	while ($_=&$getline) {
	  last if /^endBIBs/;
	  $bib.=$_;
	}
      } else {
	$putback->($tmp);
      }
      $url=~s/^\s+//; $url=~s/\s+$//;
      $bib=~s/^\s+//; $bib=~s/\s+$//;
      $db{$n}->{url}=$url;
      $db{$n}->{bib}=$bib;
    }
    close F;
  }
}

######################################################################

# Write to the file
sub write_db {
  # Make a backup copy first
  copy($file, "${file}.bak");
  open F, ">${file}.new"
    or die "Error opening ${file}.new for writing: $!\n";
  foreach my $k (sort keys %db) {
    next unless $k;
    # Remove DOS EOLs that Netscape inserts in text fields. Ugh.
    $db{$k}->{$_} =~ tr /\r//d foreach (keys %{$db{$k}});
    print F "$k,";
    print F join(",", @{$db{$k}}{qw(das dcs dcm flg com)})."\n";
    print F "URLs:\n".$db{$k}->{url}."\nendURLs\n" if $db{$k}->{url};
    print F "BIBs:\n".$db{$k}->{bib}."\nendBIBs\n" if $db{$k}->{bib};
  }
  close F;
  rename "${file}.new", $file
    or die "Error renaming ${file}.new to $file: $!\n";
}

######################################################################

# View detailed information about an IDS.
sub view {
  my $n=shift;
  Delete('view');
  print h1("Detailed information for $n");
  if (exists($db{$n})) {
    print a({href => my_url(entry2 => $n)},
	    font({size=>"-1"}, "Edit this entry")),
	      "&nbsp;&nbsp;&nbsp",
	  a({href => my_url(delete => $n)},
	    font({size=>"-1"}, "Delete this entry")),p;
    foreach my $f (grep { $db{$n}->{$_} } @fields) {
      print b("$fields{$f}: ");
      if ($f eq 'url') {
	print blockquote(join(br,
			      map { a({href => $_}, $_) }
			      split /\n+/, $db{$n}->{$f})),p;
      }
      elsif ($f eq 'bib') {
	my $b=$db{$n}->{$f};
	find_uris($b, sub { a({href => shift}, shift) });
	print pre($b),p;
      }
      else {
	print join(", ", split /&/, $db{$n}->{$f}), br;
      }
    }
  } else {
    print "Sorry, I have no information about that IDS.",p;
  }
  print hr, a({href => my_url()}, "Go back to the main screen");
  print end_html;
}

######################################################################

# Initialize a record
sub init_record {
  my $n=shift;
  $db{$n}={};
  foreach (@fields) { $db{$n}->{$_} = "" }
}

######################################################################

# Put the values of a (possibly) existing entry into the input
# fields for editing.
sub edit {
  # 'entry2' is the name typed in the text field, if any, and 'entry'
  # is the name selected using the radio buttons. The text field takes
  # precedence if it has a non-empty value, but we empty it later on,
  # transferring its value to 'entry' (so that the appropriate radio
  # buttons become selected if the IDS already exists).
  my $n=param('entry2') || param('entry');
  Delete('entry2');
  # If the name as given does not exist, first do a case-insensitive
  # search, and if that does not work, show a page with all the systems
  # that match.
  if (!exists($db{$n}) && !param('nosearch')) {
    my @search;
    if (@search=grep { lc($_) eq lc($n) } keys %db) {
      $n=$search[0];
    }
    elsif (@search=grep { /$n/i } sort keys %db) {
      if (@search == 1) {
	$n=$search[0];
      }
      else {
	print h1("Multiple matches found.");
	print "Please select one:",p;
	print ul(li([map { a({href => my_url(entry => $_)}, $_) }
		     casei_sort(@search)]),
		 li(a({href => my_url(entry => $n, nosearch => 1)}, $n),
		    "(Create a new entry with the name you typed)"));
	Delete('entry');
	print hr, a({href => my_url()},
		    "Cancel and go back to the main screen");
	print end_html;
	exit;
      }
    }
    # else $n remains as is
  }
  param('name', $n);
  if (exists($db{$n})) {
    foreach my $a (qw(das dcs com url bib)) {
      param($a, $db{$n}->{$a});
    }
    param('dcm', split(/\&/, $db{$n}->{dcm}||""));
    param('flg', split(/\&/, $db{$n}->{flg}||""));
  }
  param('entry', $n);
}

######################################################################

# Edit the entry (alphabetically) next from the one in param('name')
sub editnext {
  my $n=param('name');
  return unless $db{$n};
  my $prevfound;
  my $sys;
  foreach (casei_sort(keys %db)) {
    $sys=$_, last if $prevfound;
    $prevfound=1 if $_ eq $n;
  }
  param('entry2', $sys);
  edit();
}

######################################################################

# Enter new (or modified) information into the in-memory database.
sub enter {
  my $n=param('name');
  $lastadded=$n;
  init_record($n);
  # Get information from the fields
  foreach my $a (qw(das dcs com url bib)) {
    $db{$n}->{$a}=param($a) if param($a);
    $db{$n}->{$a}=~s/^\s+//; $db{$n}->{$a}=~s/\s+$//;
  }
  # These return lists because they are checkbox groups.
  $db{$n}->{dcm}=join("&", param('dcm')) if param('dcm');
  $db{$n}->{flg}=join("&", param('flg')) if param('flg');
}

######################################################################

# Delete a record
sub remove {
  my $n=shift;
  if (exists($db{$n})) {
    delete $db{$n};
    write_db();
    print h1("Entry for $n has been deleted");
  }
  else {
    print h1("Error");
    print "I did not have a record for $n. Nothing has been deleted.",p;
  }
  print hr, a({href => my_url()}, "Go back to the main screen");
  print end_html;
}

######################################################################

# Return my own URL, possibly setting some additional parameters
# Any parameters whose names start with a hyphen are deleted before
# generating the URL, and then reset to their old values.
sub my_url {
  my %params=@_;
  my %oldvals=();
  my ($k,$v);
  while (($k,$v) = each %params) {
    if ($k =~ /^-(.*)$/) {
      my $p=$1;
      $oldvals{$p}=$v;
      Delete($p);
    }
    else {
      param($k, $v);
    }
  }
  my $url=self_url();
  # Delete parameters we added...
  while ($k = each %params) {
    Delete($k) unless $k =~ /^-/;
  }
  # ...and restore those we deleted.
  param($_, $oldvals{$_}) foreach (keys %oldvals);
  return $url;
}
