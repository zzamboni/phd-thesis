\chapter {CONCLUSIONS, SUMMARY AND FUTURE WORK}
\label{chap;conclusions}


\section{Conclusions}
\label{sec:conclusions}


Throughout their history, \idss have been designed with different
algorithms and structures for detection. They started by being
host-based~\citep{Denning-Intrusion-Detection}, later evolved into
network-based systems~\citep[e.g.][]{Heberlein:90-2}, and in the later
years they have tended towards a distributed combination of the
two~\citep[e.g.][]{porras.neumann:emerald}. However, during all that
evolution, the sources of information used by \idss have remained
essentially unchanged: audit trails and network traffic. A few notable
exceptions have used other sources of
information~\citep[e.g.][]{hofmeyr98:intrus_detec_sequen_system_calls,kim-spaf94:desig_implem_tripw},
but even those were designed with specific applications in mind and
using only a limited set of data.

The data sources used by most \idss to date have one main limitation:
they reflect the behavior of the system being monitored, but are
separate from it. Because of this, we consider them as indirect data
sources. These data sources have limitations in the timeliness,
completeness and accuracy of the data that they provide. They make the
\ids vulnerable to attacks on several fronts, including reliability
and denial-of-service. Even when direct data sources have been used by
some \idss~\citep[e.g.][]{spafford00:intrus_detec_auton_agent}, the
sensors used to collect the information have been external to the
objects being monitored; therefore still subject to multiple forms of
attack.

The problems and limitations that have been encountered in \idss have
indicated that the best place to collect data about the behavior of a
program or system is at the point where the data is generated or
used.

In this dissertation, we have proposed an architecture based on using
internal sensors built into the code of the programs that are
monitored and are able to extract information from the places in which
it is generated or used. Furthermore, by expanding those internal
sensors with decision-making logic, we showed an application of
embedded detectors to build an \ids. If necessary, this \ids can
operate without any external components (components that are separate
from the ones being monitored), other than those necessary to read the
alerts produced.

To demonstrate the feasibility of this architecture and to learn more
about its needs and capabilities, we described a specific
implementation in the OpenBSD operating system. In its original form,
this implementation detects 130 specific attacks, and through the
experience acquired with those, 20 generic detectors were implemented
that have the capability of detecting previously unknown attacks, as
demonstrated by the detection experiments performed.

This dissertation provides an architectural and practical framework in
which future study of internal sensors and embedded detectors in \id
can be based. Other research projects are already using this framework
for the study of novel techniques for both host-based and distributed
\id~\citep{early01:qual_ii,gopalakrishna01:qual_ii}, and considerable
further study on the use of internal sensors is possible.

\CB{Both Thesis Hypotheses (Section~\ref{sec:thesis-stmt}) were shown
  to be true. First, it is possible to build an \ids using internal
  sensors while maintaining fidelity, reliability, and reasonable
  resource usage; although in this last respect, we concluded that the
  specifics are dependent on the implementation. Second, internal
  sensors can be used to detect not only known attacks for which they
  are specifically designed, but also new attacks by looking at
  generic indicators of malicious activity.}

This dissertation also provides a classification of data source types
for \idss, and a description of the characteristics and types of
internal sensors and embedded detectors. Furthermore, its
implementation provides specific insight into the types of data that
are more useful in the detection of attacks using internal sensors,
and into places in a system where those sensors can best be placed to
make them efficient and reliable.

Although the implementation cost of the prototype described in this
dissertation was high, the advantages of having internal sensors
available on a system are numerous. We expect that our work will
provide guidance and encouragement for their integration in future
systems since their design, which will lead to considerable reductions
in their implementation cost and in their impact on performance and
size.

\CB{In a sense, our approach differs little from the error-checking
  and careful programming that should exist in any well-maintained
  system. But the use of code embedded into the operating system and
  the programs not only for prevention of problems, but for generation
  of data, is an important step in the development of \idss that can
  provide complete and accurate information about the behavior of a
  host. The collection of data, even if it is about actions that do
  not constitute an immediate risk, may provide information about
  other attacks against which hosts are not protected yet.}

\section{Summary of main contributions}
\label{sec:summary}

\begin{itemize}
\item Provided a classification of data sources for \id, and of the
  mechanisms used for accessing them.
  
\item Described the properties of internal sensors in their use for
  \id, and an architecture for building \idss based on internal
  sensors and specialized sensors named \emph{embedded detectors}.
  
\item Implemented a prototype \ids using the architecture described,
  showing that it is possible to use internal sensors to perform \id
  while maintaining most of the desirable properties described in
  Section~\ref{sec:desirable-characteristics-ids}.
  
\item Showed that through the implementation of a ``large enough''
  number of specific detectors, it was possible to identify patterns
  and concepts to be used in generic detectors, confirming the notion
  that attacks against computer systems tend to cluster around
  certain vulnerabilities. Once these core problems are identified, it
  is possible to build generic detectors for them.

\item Showed that the prototype implemented is able to detect
  previously unknown attacks through the use of properly designed
  generic detectors.
  
\CB{\item Showed that it is possible to build a general-purpose \ids
  based on obtaining the data needed for the detection, instead of
  relying on the data provided by the operating system and the
  applications.}

\item Showed that it is possible for an \ids built using internal
  sensors to have acceptable impact on the host.
  
\item Showed that careless implementation can cause sensors to have a
  large impact on the host. When using internal sensors,
  implementation issues are significantly more important than when
  using traditional external detectors.
  
\item Collected information about the placement and types of data most
  frequently used by detectors; data therefore most likely to be
  useful in the development of future detectors and sensors.

\item Identified types of vulnerabilities most frequently encountered, 
  both through the development of the prototype and in the detection
  testing performed.

\item Showed that internal sensors and embedded detectors can add
  significant detection capabilities to a system while increasing its
  size only by a small fraction.
  
\item By combining concepts from source code instrumentation and \id,
  we showed that it is possible to build an \ids that can perform \id
  at multiple levels (operating system, application, network) and in
  multiple forms (signature-based, anomaly-based) to increase its
  coverage.

%\item Showed that the same detectors can be used to detect intrusions
%  applicable to multiple architectures and programs even when they
%  do not share the same code base. This confirms the existence of some 
%  fundamental vulnerabilities that manifest themselves in multiple
%  systems.

\end{itemize}



\section{Future work}
\label{sec:future-work}

The work presented in this dissertation has explored the basic
concepts of using internal sensors for \id by showing their
feasibility. However, there is a considerable amount of work that
needs to be done to further study and characterize their properties.

This dissertation focuses on the use of internal sensors in a single
host. We consider ESP as a distributed \id architecture because the
sensors operate independently in multiple components, but work is
needed to show the feasibility and characteristics of using internal
sensors in an \ids that spans multiple hosts. Some work is already
underway~\citep{gopalakrishna01:qual_ii} to study the mechanisms that
could be used in such a system in a way that prevents overloading of
both communication channels and coordination components.

The performance tests showed that implementation decisions can
severely affect the performance impact of the sensors. In this
respect, practical work is necessary to perform optimization and
reevaluation of the detectors. Further study is necessary to identify
the factors of a detector that result in the largest processing
overhead, and in the best ways of reducing those factors.

In terms of the detection capabilities of internal sensors and
embedded detectors, the results presented in
Section~\ref{sec:detection-testing} show that they have the
possibility of detecting a significant percentage of new attacks.
Longer-term testing may help in fully understanding and possibly
modeling their capabilities and limitations.  A formal
characterization of the detectors in relationship to the types of
attacks encountered would provide firmer prediction capabilities,
\CB{and help ensure consistency and completeness of the data provided
  by the detectors}. A probabilistic model that links the ``ease of
detection'' of each type of vulnerability with the expected occurrence
of each category in new attacks (as implied by the percentages of
detection and total occurrence of each category in
Figure~\ref{fig:detexp-krsul-original-final}) could be useful in
predicting the detection capabilities of embedded detectors. Such a
model could also be related to the expected effect that improvements
to the detectors have in the detection capabilities (as shown in
Figure~\ref{fig:detexp-krsul-original-final-plot}) to determine
cost-effective policies for sensor and detector maintenance and
upgrading.

We showed that by implementing detectors for a relatively small
fraction of the entries in the CVE database, we were able to implement
generic detectors capable of detecting a significant percentage of new
attacks. Future work could explore improving the detection rate for
new attacks by implementing detectors for a larger number of CVE
entries.

Once a host is instrumented with internal sensors, it might be
possible to explore new detection capabilities that would have been
to expensive or complex to implement using traditional \idss. One such 
possibility is that of \emph{outbound \id{}}, in which internal sensors
could provide enough information to detect malicious activity at its
origin, so that the burden of \id can be placed not
only on the victims, but also on the potential attackers such as the
hosts at Internet Service Providers.

\citet{ErlSch99} described the use of \emph{reference monitors} that
are automatically generated. In this dissertation, the internal
sensors and embedded detectors are individually hand-coded. Using the
information gained about the types of data that need to be collected,
it may be possible to explore the possibility of automatically
generating those sensors in a policy directed fashion. Another
possibility would be the automatic generation of components that could 
be used by programmers to insert sensors and detectors in their code.

As a design decision, during this work we avoided using the embedded
detectors to stop an attack once its detected. However, this is a
clear application for embedded detectors because of their localization
and their ability to perform early detection.  Automatic reaction to
intrusions has not been widely explored in practice because of the
dangers it presents (a false alarm can result in the interruption or
modification of legitimate activity), but embedded detectors would be
an ideal mechanism for implementing it. \CB{The feasibility of this
  task has been shown in the implementation of the pH
  system~\citep{somayaji00:autom_respon_using_system_call_delay},
  which uses internal sensors to perform both detection and reaction
  to attacks.}

\CB{This dissertation has explored the feasibility of extracting
  information about the behavior of a computer system that is more
  complete and reliable than any data that had been available before
  to \idss. This availability opens multiple possibilities for future
  exploration and research, and may lead to the design and development
  of more efficient, reliable and effective \idss.}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "~/Thesis/Dissertation/thesis"
%%% End: 
