
\subsection{Embedded detectors for network-based attacks}
\label{sec:networkdetectors}

We implemented a number of embedded detectors for common network-based
attacks. We use the term \emph{network-based attacks} to encompass
those that exploit both low-level IP vulnerabilities and network-based
vulnerabilities as described by~\citet{daniels99:ip-vuln}. In this
section, we describe this implementation and the results obtained.

\subsubsection{Detectors implemented}
\label{sec:network-detectors-implemented}

We chose network-based attacks because several interesting attacks of
this type have appeared over the last few years. Also, they are
the type of attacks that \idss using network-based data collection
usually detect, and our implementation shows how effective embedded
detectors can be for these attacks.

Table~\ref{tab:network-detectors-table} lists the detectors that were
implemented for network-based attacks during the initial study phase.
\input{tables/network-detectors-table}

In the next sections, we describe some representative attacks. We show
the code of the corresponding detectors (in many cases the code has
been reformatted for space) and explain where they have been placed
within the operating system. We will see that detectors are short and
simple, yet provide advanced detection capabilities.

The lines of code added or modified by a detector have been
highlighted in each code section. The detectors have been wrapped in
\texttt{\#ifdef} directives and in an \fnc{if} clause, so they can be
disabled both at compile time and at run time. We explored the
possibility of integrating the run-time control variables to the
kernel parameters mechanism available in OpenBSD through which some
kernel parameters can be modified at run time.  The ability to disable
the detectors at runtime may not be desirable in a production system
because it offers the possibility for an attacker to disable the
detectors if he manages to obtain sufficient privileges in the system.
However, for the purposes of testing, the capability of enabling and
disabling detectors at runtime was considered appropriate.

\subsubsection{Stateless Detectors}

Twelve of the 16 detectors in Table~\ref{tab:network-detectors-table}
are stateless.  Those detectors test if an attack condition is met and
call the alert mechanism.  They use information from the network stack
and are placed within its execution path.
% ZZ - I don't understand this paragraph. What do you mean?
%The condition is directly
%related to the vulnerability and has not been checked in vulnerable
%systems. The attack always violates the semantics of a correctly
%implemented TCP/IP stack.
An example of this type of attack is the Land~\cite{CA-1997-28} attack
(CVE-1999-0016). It consists of a TCP SYN packet sent to an open port
with the source address and port set to destination address and port.
OpenBSD filters those packets when processing SYN packets in the
TCP\_LISTEN state and drops them. The detector exploits this and is
placed before the packet drop, so it is effectively only a single
statement (with additional code for detector management).

\begin{code}
case TCPS_LISTEN: \{
\ldots
  if (ti->ti_dst.s_addr == ti->ti_src.s_addr) \{
\textbf{/* ESP */
#ifdef ESP_CVE_1999_0016
  if (esp.sensors.land)
      esp_logf("CVE-1999-0016: LAND attack\n");
#endif}
      goto drop;
  \}
  \ldots
\}
\end{code}

The CVE-1999-0103 (Echo-chargen denial-of-service
attack~\citep{CA-1996-01}) detector was implemented within the
\prog{inetd}~\cite{man:inetd} program and not in the kernel. Also, it
is longer than other detectors because it has to query additional
information that is not readily available outside the kernel.

In this group of detectors we found the first instance of an
``implemented-by'' relationship. The PIX DoS
attack~\citep{cisco98:cisco_pix_cbac_fragm_attac} (CVE-1999-0157)
exploits the same vulnerability (failure to handle a special case of
overlapping IP packets) as Teardrop~\citep{CA-1997-28} (CVE-1999-0052)
but with a variation to bypass a PIX firewall. In this case, the ESAM
count indicates the \CB{number of statements added to or modified in
  CVE-1999-0052 to implement the detector for CVE-1999-0157, and the
  BOCAM count of 1 indicates that those statements are contiguous.}

SYN flooding~\citep{schuba97:synkill} is a denial-of-service attack
based on exhaustion of the resources allocated in a host for half-open
TCP connections. The detector for SYN flooding was implemented as
stateless. OpenBSD does resource allocation for half-open connections
and drops old connections after a threshold has been reached. The
detector triggers when such a connection is dropped.  This shows an
advantage of embedded detectors: they can use the defense mechanisms
of the operating system itself and combine them with detection.

Other attacks are ICMP unreachable messages (CVE-1999-0214) and ICMP
redirects (CVE-1999-0265), both of which allow an attacker to cause a
denial-of-service attack by faking ICMP control messages.  The problem
is that those faked ICMP messages may be indistinguishable from
legitimate messages created by hosts at the end points of the
connection or by interior routers. These type of attacks are inherent
to the design of TCP/IP. OpenBSD tries to protect itself from
malicious messages with extensive checks against its local state and
we placed the detectors after those, i.e.  that packets that are
accepted by OpenBSD will not raise an alarm, while rejected will.
Nevertheless cleverly forged packets still may exploit those
vulnerabilities.

\subsubsection{Stateful Detectors}

Stateful detectors accumulate data about events that indicate attacks.
In some of our detectors, a separate timer routine reads these data and
triggers an alarm if a threshold has been met. Two typical examples
are the Smurf and Fraggle~\cite{CA-1998-01,huegen00:smurfing} attacks.
They try to flood the host with packets of a certain type and make it
unavailable to its users.

Those attacks rely on traffic amplification mechanisms.  Traffic
amplification is based on mechanisms that generate a response
significantly larger than the request that originates it.  This
enables a single attacker to generate the amount of traffic necessary
to exceed the victim's capacity. Stateless detectors may detect the
packets that use those mechanisms to generate the attack. However,
often the attacked site and the amplifying site are different, so a
different detector for the victim host is necessary. Identifying the
vulnerability at the amplifying site can assist in tracing the attack.

The Smurf attack~\citep{CA-1998-01} sends ICMP ECHO\_RESPONSE packets.
Those do not differ from legitimate packets (for example, in response
to a \cmd{ping} command~\cite{man:ping}) except that there is no
program expecting them.  For implementing the detector, we assumed the
semantics of the \prog{ping} program, that stores its Process ID in
the ICMP ID field to identify its replies. Based on that technique, we
store the Process ID of all ICMP raw sockets in the socket data
structure when they are created:
\begin{code}
case PRU_ATTACH:
\ldots
\textbf{/* ESP */
#ifdef ESP_CVE_1999_0513
   if (esp.sensors.smurf && ((long) nam) == IPPROTO_ICMP)
      so->so_pgid = curproc->p_pid;
#endif}
\end{code}
We check this information at arriving ICMP echo replies and increase a
counter for unrequested echo replies if there is no matching socket
(this is done in the \fnc{esp\_smurf()} function, not shown).
\begin{code}
case ICMP_ECHOREPLY:
\textbf{/* ESP */
#ifdef ESP_CVE_1999_0513
  if (esp.sensors.smurf) \{
    if (esp_smurf(ip, icp))
      goto freeit;
    goto raw;
  \}
#endif}
\end{code}

The technique used above shows another advantage of embedded
detectors: additional information can be made available when necessary
for the purposes of detection.

The alarm for Smurf is rate-limited. A legitimate use of \cmd{ping}
will probably be interrupted when there are still echo reply packets
in the network to be delivered to the host, and those packets should
not raise an alarm although they do match the signature. A network
layer timer that runs for three seconds examines the counter and
raises an alarm only if it exceeds a threshold.

\CB{Port scanning~\citep{Fyodor97} is a probing technique used to
  determine what ports are open on a host, and is commonly performed
  as an exploration phase by an attacker. For this reason, although
  port scans themselves are not attacks, we consider it desirable to
  detect them. We implemented a port scan detector} that reacts to all
known types of port scanning techniques (including stealth and slow
scans) by using the state of the network stack. Also, it has more
advanced monitoring and reporting capabilities because it reports
multiple probes as one scan and identifies its type.

The NetBSD race attack detector (CVE-1999-0396) is a special case of
the port scan detector and uses its reporting routine. For this
reason, CVE-1999-0396 is ``detected-by'' ESP-PORTSCAN.

A detailed description of all the detectors for network-based attacks
is available~\citep{kerschbaum00:network-embedded-sensors}.

\subsubsection{Testing the detectors}
\label{sec:testing}

A test suite of exploit programs was assembled to test the detectors.
The exploit programs were acquired preferably from the same sources
that published the vulnerabilities when they made them available.  If
they were not available or not working, we wrote our own exploits
according to the descriptions. The test suite was run supervised from
a remote machine on the same local area network (LAN) and all attacks
were detected reliably.

An independent tester ran the same set of attacks. The attacks were
run over the campus network, with different network technologies and
possibly even filtering in between. The results were that only a small
number of attacks arrived at the target. This experience shows that
most attacks are of rather low quality and are dependent on the
network environment. The packet log shows that all received attacks
were detected. The test was repeated from a machine on the same LAN
and the results match those of the supervised test.

In the testing period the host reported some attacks not generated as
a controlled experiment, notably port scans.  To verify their
correctness, they were compared to the packet log and all could be
verified as real events.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "~/Thesis/Dissertation/thesis"
%%% End: 
