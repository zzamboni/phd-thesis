
\chapter {INTRODUCTION}
\label{chap:introduction}

It is feasible to perform computer \id at the host level for both
known and new attacks using internal sensors and embedded detectors
with reasonable CPU and size overhead. The present document discusses
this assertion in detail, and describes the work done to show its
validity.

%----------------------------------------------------------------------

\section{Background and problem statement}
\label{sec:problem}

The field of \id has received increasing attention in recent years.
One reason is the explosive growth of the Internet and the large
number of networked systems that exist in all types of organizations.
The increased number of networked machines has led to a rise in
unauthorized activity~\citep{cert-statistics}, not only from external
attackers, but also from internal sources such as disgruntled
employees and people abusing their privileges for personal
gain~\citep{power99:csi_fbi_computer_crime_survey}.

In the last few years, a number of \idss have been developed both in
the commercial and academic sectors. These systems use various
approaches to detect unauthorized activity and have given us some
insight into the problems that still have to be solved before we can
have \idss that are useful and reliable in production settings for
detecting a wide range of intrusions.

Most of the existing \idss have used central data analysis
engines~\mbox{\citep[e.g.][]{crosbie.dole.al:idiot-manual,lunt:92-kumar}} or
per-host data collection and analysis
components~\mbox{\citep[e.g.][]{Heberlein:90-2,porras.neumann:emerald}}
that are implemented as separate processes running on one or more of
the machines in a network.  In their design and implementation, all of
these approaches are subject to a number of problems that limit their
scalability, reliability and resistance to attacks.

At CERIAS (Center for Education and Research in Information Assurance
and Security) at Purdue University, we have developed a monitoring
technique called \cpt{internal sensors} based on source code
instrumentation. This technique allows the close observation of data
and behavior in a program. It can also be used to implement \idss that
perform their task in near real-time, that are resistant to attacks
and that impose a reasonably low overhead in the hosts, both in terms
of memory and CPU utilization.

This dissertation describes the concept of using internal sensors for
building an \id framework at the host level, their characteristics and
abilities, and experimental results in an implementation.

%----------------------------------------------------------------------

\section{Basic concepts}
\label{sec:concepts}

First, we introduce some basic concepts on which this dissertation is
based.


\subsection{Intrusion detection}
\label{sec:Intrusion-Detection}

\Id has been defined as ``the problem of identifying individuals who
are using a computer system without authorization (i.e., `crackers')
and those who have legitimate access to the system but are abusing
their privileges (i.e., the `insider threat')''~\citep{ID:MuHeLe94}.

We add to this definition the identification of \emph{attempts} to use
a computer system without authorization or to abuse existing
privileges.  Therefore, our working definition of \emph{intrusion}
matches the one given by~\citet{Heady:90}:
\begin{wdefinition}{\protect\cbstart Intrusion\protect\cbend}
  Any set of actions that attempt to compromise the integrity,
  confidentiality, or availability of a computer resource.
\end{wdefinition}
\CB{This definition disregards the success or failure of those
  actions, so it also corresponds to attacks against a computer
  system. In the rest of this dissertation we use the terms
  \emph{attack} and \emph{intrusion} interchangeably.}

The definition of intrusion results in our working definition of \id:
\begin{wdefinition}{\protect\cbstart Intrusion detection\protect\cbend}
  The problem of identifying actions that attempt to compromise the
  integrity, confidentiality, or availability of a computer resource.
\end{wdefinition}

The definition of the word \emph{intrusion} in an English
dictionary~\citep{webster-dictionary} does not include the concept of
an insider abusing his or her privileges or attempting to do so. A
more accurate phrase to use is \emph{intrusion and insider abuse
  detection}.  In this document we use the term \emph{intrusion} to
mean both intrusion and insider abuse.

\begin{wdefinition}{\protect\cbstart Intrusion detection system\protect\cbend}
  A computer system (possibly a combination of software and hardware)
  that attempts to perform \id.
\end{wdefinition}
Most \idss try to perform their task in real time~\citep{ID:MuHeLe94}.
However, there are also \idss that do not operate in real time, either
because of the nature of the analysis they
perform~\citep[e.g.][]{kim-spaf94:desig_implem_tripw} or because they
are geared for forensic analysis (analysis of what has happened in the
past on a
system)~\citep[e.g.][]{tan:id-forensic,farmer99:forensics_class}.

The definition of an \ids does not include preventing the intrusion
from occurring, only detecting it and reporting it to an operator.
There are some
\idss~\citep[e.g.][]{cisco01:secure-ids,somayaji00:autom_respon_using_system_call_delay}
that try to react when they detect an unauthorized action. This
reaction usually includes trying to contain or stop the damage, for
example, by terminating a network connection.

\Idss are usually classified as host-based or
network-based~\citep{ID:MuHeLe94}. Host-based systems base their
decisions on information obtained from a single host (usually audit
trails), while network-based systems obtain data by monitoring the
traffic in the network to which the hosts are connected.

%In the
%following section, we propose a more detailed classification that
%differentiates between collection and processing of the data used by
%an \ids.


\subsection{Desirable characteristics of an \ids}
\label{sec:desirable-characteristics-ids}

The following characteristics are ideally desirable for an \ids (based
on the list provided by \citet{ID:Crosbie95}):

\begin{enumerate}
  
\item\label{dc:continually} It must \emph{run continually} with
  minimal human supervision.
  
\item\label{dc:fault} It must be \emph{fault tolerant}:
  \begin{enumerate}
  \item\label{dc:fault-recover} The \ids must be able to recover from
    system crashes, either accidental or caused by malicious activity.
  \item\label{dc:fault-state} After a crash, the \ids must be able to
    recover its previous state and resume its operation unaffected.
  \end{enumerate}
  
\item\label{dc:subversion} It must \emph{resist subversion}:
  \begin{enumerate}
  \item\label{dc:subversion-difficulty} There must be a significant
    difficulty for an attacker to disable or modify the \ids.
  \item\label{dc:subversion-detect} The \ids must be able to monitor
    itself and detect if it has been modified by an attacker.
  \end{enumerate}
  
\item\label{dc:overhead} It must impose a \emph{minimal overhead} on
  the systems where it runs to avoid interfering with their normal
  operation.
  
\item\label{dc:config} It must be \emph{configurable} to accurately
  implement the security policies of the systems that are being
  monitored.
  
\CB{\item\label{dc:deployment} It must be \emph{easy to deploy}. This can
  be achieved through portability to different architectures and
  operating systems, through simple installation mechanisms, and by
  being easy to use and understand by the operator.}

\item\label{dc:adapt} It must be \emph{adaptable} to changes in system
  and user behavior over time. For example, new applications being
  installed, users changing from one activity to another, or new
  resources being available can cause changes in system use patterns.
  
\item\label{dc:detect} It must be \emph{able to detect attacks}:
  \begin{enumerate}
  \item\label{dc:detect-falsepos} The \ids must not flag any
    legitimate activity as an attack (false positives).
  \item\label{dc:detect-falseneg} The \ids must not fail to flag any
    real attacks as such (false negatives). It must be difficult for
    an attacker to mask his actions to avoid detection.
  \item\label{dc:detect-realtime} The \ids must report intrusions as
    soon as possible after they occur.
  \CB{\item\label{dc:detect-generality} The \ids must be general enough to 
    detect different types of attacks.}
  \end{enumerate}

\end{enumerate}

We will refer to these characteristics throughout this dissertation
for description of different \id architectures and systems, including
those developed for this dissertation.

%----------------------------------------------------------------------

\section{Problems with existing \idss}
\label{sec:problems-with-existing-idss}

Most existing \idss (for example, those surveyed
by~\citet{axelsson98:resear_intrus_detec_system}, plus
others~\citep[e.g.][]{spafford00:intrus_detec_auton_agent,barnett97:intrus_detec_dynam_softw_agent})
suffer from at least two of the following problems:

First, the information used by the \ids is obtained from audit trails
or from packets on a network. Data has to traverse a longer path from
its origin to the \ids, and in the process can potentially be
destroyed or modified by an attacker. Furthermore, the \ids has to
infer the behavior of the system from the data collected, which can
result in misinterpretations or missed events. We refer to this as the
\emph{fidelity} problem. It corresponds to a failure to properly
address desirable characteristic~\#\ref{dc:detect}.

Second, the \ids continuously uses additional resources in the system
it is monitoring even when there are no intrusions occurring, because
the components of the \ids have to be running all the time.  This is
the \emph{resource usage} problem and corresponds to a failure in
addressing desirable characteristic~\#\ref{dc:overhead}.

Third, because the components of the \ids are implemented as separate
programs, they are susceptible to tampering. An intruder can
potentially disable or modify the programs running on a system,
rendering the \ids useless or unreliable. This is the
\emph{reliability} problem and corresponds to a failure to address
desirable characteristics \#\ref{dc:continually}, \#\ref{dc:fault}
and~\#\ref{dc:subversion}.

In this dissertation, we describe a mechanism that addresses all three
of these problems and has several other desirable characteristics.


%\section{Definitions used in this dissertation}

%Common definitions. Some others will be introduced in their appropriate
%context.

%\section{Motivation}

\input{thesis-stmt}

\input{organization}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "~/Thesis/Dissertation/thesis"
%%% End: 
