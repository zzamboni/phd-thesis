%%
%% This is file `thesis.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% thesis.dtx  (with options: `thesis')
%% 
%% This is a generated file.
%% 
%% Copyright 1993 1994 1995 1996 1997
%% The LaTeX3 Project and any individual authors listed elsewhere
%% in this file.
%% 
%% For further copyright information, and conditions for modification
%% and distribution, see the file legal.txt, and any other copyright
%% notices in this file.
%% 
%% This file is part of the LaTeX2e system.
%% ----------------------------------------
%%   This system is distributed in the hope that it will be useful,
%%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%% 
%%   For error reports concerning UNCHANGED versions of this file no
%%   more than one year old, see bugs.txt.
%% 
%%   Please do not request updates from us directly.  Primary
%%   distribution is through the CTAN archives.
%% 
%% 
%% IMPORTANT COPYRIGHT NOTICE:
%% 
%% You are NOT ALLOWED to distribute this file alone.
%% 
%% You are allowed to distribute this file under the condition that it
%% is distributed together with all the files listed in manifest.txt.
%% 
%% If you receive only some of these files from someone, complain!
%% 
%% 
%% Permission is granted to copy this file to another file with a
%% clearly different name and to customize the declarations in that
%% copy to serve the needs of your installation, provided that you
%% comply with the conditions in the file legal.txt.
%% 
%% However, NO PERMISSION is granted to generate or to distribute a
%% modified version of this file under its original name.
%% 
%% You are NOT ALLOWED to change this file.
%% 
%% 
%% MODIFICATION ADVICE:
%% 
%% If you want to customize this file, it is best to make a copy of
%% the source file(s) from which it was produced.  Use a different
%% name for your copy(ies) and modify the copy(ies); this will ensure
%% that your modifications do not get overwritten when you install a
%% new release of the standard system.  You should also ensure that
%% your modified source file does not generate any modified file with
%% the same name as a standard file.
%% 
%% You can then easily distribute your modifications by distributing
%% the modified and renamed copy of the source file, taking care to
%% observe the conditions in legal.txt; this will ensure that other
%% users can safely use your modifications.
%% 
%% You will also need to produce your own, suitably named, .ins file to
%% control the generation of files from your source file; this file
%% should contain your own preambles for the files it generates, not
%% those in the standard .ins files.
%% 
%% The names of the source files used are shown above.
%% 
%% 
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{thesis}
       [1998/01/25 v0.1 Purdue thesis document class]
\if@compatibility\else
\DeclareOption{a4paper}{\PassOptionsToClass{a4paper}{report}}
\DeclareOption{a5paper}{\PassOptionsToClass{a5paper}{report}}
\DeclareOption{b5paper}{\PassOptionsToClass{b5paper}{report}}
\DeclareOption{letterpaper}{\PassOptionsToClass{letterpaper}{report}}
\DeclareOption{legalpaper}{\PassOptionsToClass{legalpaper}{report}}
\DeclareOption{executivepaper}{\PassOptionsToClass{executivepaper}{report}}
\DeclareOption{landscape}{\PassOptionsToClass{landscape}{report}}
\DeclareOption{10pt}{\PassOptionsToClass{10pt}{report}}
\DeclareOption{11pt}{\PassOptionsToClass{11pt}{report}}
\DeclareOption{12pt}{\PassOptionsToClass{12pt}{report}}
\DeclareOption{draft}{\PassOptionsToClass{draft}{report}}
\DeclareOption{final}{\PassOptionsToClass{final}{report}}
\DeclareOption{leqno}{\PassOptionsToClass{leqno}{report}}
\DeclareOption{fleqn}{\PassOptionsToClass{fleqn}{report}}
\DeclareOption{openbib}{\PassOptionsToClass{openbib}{report}}
\ExecuteOptions{letterpaper,10pt,final}
\PassOptionsToClass{oneside,onecolumn,titlepage,openany}{report}
\ProcessOptions
\LoadClass{report}
\fi
\newcommand\oneskip{1.0}
\newcommand\twoskip{1.5}
\newcommand\singlespace{\renewcommand\baselinestretch{\oneskip}\normalsize}
\newcommand\doublespace{\renewcommand\baselinestretch{\twoskip}\normalsize}
\renewcommand\baselinestretch{\twoskip}
\newdimen\@leftmargin
\setlength\@leftmargin{1.5in}
\newdimen\@rightmargin
\setlength\@rightmargin{1.0in}
\newdimen\@topmargin
\setlength\@topmargin{1.0in}
\newdimen\@headermargin
\setlength\@headermargin{0.5in}
\newdimen\@bottommargin
\setlength\@bottommargin{1.25in}
\newdimen\@chapterheadskip
\setlength\@chapterheadskip{2.0in}
\newdimen\@appendicesheadskip
\setlength\@appendicesheadskip{1.0in}
\setlength\headsep{\@topmargin}
\addtolength\headsep{-\@headermargin}
\addtolength\headsep{-\headheight}
\if@compatibility
  \setlength\textwidth{6in}
\else
  \setlength\textwidth{\paperwidth}
  \addtolength\textwidth{-\@leftmargin}
  \addtolength\textwidth{-\@rightmargin}
  \@settopoint\textwidth
\fi
\if@compatibility
  \setlength\textheight{8.75in}
\else
  \setlength\@tempdima{\paperheight}
  \addtolength\@tempdima{-\@topmargin}
  \addtolength\@tempdima{-\@bottommargin}
  \divide\@tempdima\baselineskip
  \@tempcnta=\@tempdima
  \setlength\textheight{\@tempcnta\baselineskip}
\fi
\addtolength\textheight{\topskip}
\if@compatibility
\else
  \if@twoside
  \else
    \setlength\oddsidemargin{\@leftmargin}
    \addtolength\oddsidemargin{-1in}
    \setlength\evensidemargin{\@leftmargin}
    \addtolength\evensidemargin{-1in}
  \fi
  \@settopoint\oddsidemargin
  \@settopoint\evensidemargin
\fi
\setlength\topmargin{\@headermargin}
\addtolength\topmargin{-1in}
\@settopoint\topmargin
\newcommand\@draftfooter{\rmfamily%
  Draft:{\scshape\ Do Not Distribute}\hfil {Date:\ \today}\hfil {Time:\now}}
\if@twoside
  \def\ps@thesisdraft{%
    \def\@oddfoot{\@draftfooter}%
    \def\@evenfoot{\@draftfooter}%
    \def\@evenhead{\rmfamily\thepage\hfil}%
    \def\@oddhead{\hfil\rmfamily\thepage}%
    \let\@mkboth\@gobbletwo
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
  }
\else
  \def\ps@thesisdraft{%
    \def\@oddfoot{\@draftfooter}%
    \def\@oddhead{\hfil\rmfamily\thepage}%
    \let\@mkboth\@gobbletwo
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
  }
\fi
\if@twoside
  \def\ps@thesis{%
    \let\@oddfoot\@empty
    \let\@evenfoot\@empty
    \def\@evenhead{\rmfamily\thepage\hfil}%
    \def\@oddhead{\hfil\rmfamily\thepage}%
    \let\@mkboth\markboth
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
  }
\else
  \def\ps@thesis{%
    \let\@oddfoot\@empty
    \def\@oddhead{\hfil\thepage}%
    \let\@mkboth\@gobbletwo
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
  }
\fi
\def\@English#1{%
  \ifcase#1\or One\or Two\or Three\or Four\or Five\or Six\or Seven\or
   Eight\or Nine\or Ten\or Eleven\or Twelve\or Thirteen\or Fourteen\or
   Fifteen\or Sixteen\or Seventeen\or Eightteen\or Nineteen\or
   Twenty\else\@ctrerr\fi}
\def\English#1{\expandafter\@English\csname c@#1\endcsname}
\renewcommand\thepart{\@English\c@part}
\newcommand*{\degree}[1]{\gdef\@degree{#1}}
\newcommand*{\degreebrief}[1]{\gdef\@degreebrief{#1}}
\newcommand*{\advisor}[1]{\gdef\@advisor{#1}}
\newcommand*{\authorname}[1]{\@authorname#1@}
\def\@authorname#1, #2@{\gdef\@author{#2 #1}
                        \gdef\@lastnamefirst{#1, #2}}
\newcommand\@thesistitlemedskip{0.15in}
\newcommand\@thesistitlebigskip{0.60in}
\if@titlepage
  \renewcommand\maketitle{%
    \begin{titlepage}%
      \let\footnotesize\small
      \let\footnoterule\relax
      \null\vfil
      \vskip \headsep
      %\vskip 30\p@
      \begin{center}
        \MakeUppercase{\@title} \par\vspace*{\@thesistitlebigskip}
        A Thesis \par\vspace*{\@thesistitlemedskip}
        Submitted to the Faculty \par\vspace*{\@thesistitlemedskip}
        of \par\vspace*{\@thesistitlemedskip}
        Purdue University \par\vspace*{\@thesistitlemedskip}
        by \par\vspace*{\@thesistitlemedskip}
        \@author \par\vspace*{\@thesistitlebigskip}
        In Partial Fulfillment of the \par\vspace*{\@thesistitlemedskip}
        Requirements for the Degree \par\vspace*{\@thesistitlemedskip}
        of \par\vspace*{\@thesistitlemedskip}
        \@degree \par\vspace*{\@thesistitlebigskip}
        \@date
    \end{center}\par
    \vfil\null
    \end{titlepage}%
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@thanks\@empty
  }
\fi
\renewcommand\part{%
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \if@twocolumn
    \onecolumn
    \@tempswatrue
  \else
    \@tempswafalse
  \fi
  \null\vfil
  \secdef\@part\@spart}
\def\@part[#1]#2{%
    \ifnum \c@secnumdepth >-2\relax
      \refstepcounter{part}%
      \addcontentsline{toc}{part}{\MakeUppercase{\thepart\hspace{1em}#1}}%
    \else
      \addcontentsline{toc}{part}{\MakeUppercase{#1}}%
    \fi
    \markboth{}{}%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \ifnum \c@secnumdepth >-2\relax
       \MakeUppercase{\partname~\thepart}
       \par
       \vskip 20\p@
     \fi
     \MakeUppercase{#2}\par}%
    \@endpart}
\def\@spart#1{%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \Huge #1\par}%
    \@endpart}
\renewcommand\chapter{%
  \if@openright\cleardoublepage\else\clearpage\fi
  \global\@topnum\z@
  \@afterindenttrue
  \secdef\@chapter\@schapter}
\def\@chapter[#1]#2{%
  \ifnum \c@secnumdepth >\m@ne
    \refstepcounter{chapter}%
    \typeout{\@chapapp\space\thechapter.}%
    \ifcase\c@appendix
      % Not in an appendix.
      \addcontentsline{toc}{chapter}%
        {\protect\numberline{\thechapter}\MakeUppercase{#1}}%
      % In appendix.
      \or\addcontentsline{toc}{chapter}{\MakeUppercase{\appendixname\ #1}}%
      % In appendices.
      \or\addcontentsline{toc}{chapter}%
        {\protect\numberline{\thechapter}\MakeUppercase{#1}}%
      \else\@ctrerr
    \fi
  \else
    \ifcase\c@appendix
      % Not in an appendix.
      \addcontentsline{toc}{chapter}{\MakeUppercase{#1}}%
      % In appendix.
      \or\addcontentsline{toc}{chapter}{\MakeUppercase{\appendixname\ #1}}%
      % In appendices.
      \or\addcontentsline{toc}{chapter}{\MakeUppercase{#1}}%
      \else\@ctrerr
    \fi
  \fi
  \chaptermark{\MakeUppercase{#1}}%
  \if@twocolumn
    \@topnewpage[\@makechapterhead{\MakeUppercase{#2}}]%
  \else
    \@makechapterhead{\MakeUppercase{#2}}%
    \@afterheading
  \fi}
\def\@makechapterhead#1{%
  \suppressfloats[t]
  \ifcase\c@appendix
    \setlength{\@tempdima}{\@chapterheadskip}%
    \or\setlength{\@tempdima}{\@chapterheadskip}%
    \or\setlength{\@tempdima}{\@appendicesheadskip}%
    \else\@ctrerr
  \fi
  \addtolength{\@tempdima}{-\headsep}%
  \addtolength{\@tempdima}{-\@topmargin}%
  \vspace*{\@tempdima}%
  {\parindent \z@ \raggedright \normalfont
   \interlinepenalty\@M
   \ifnum\c@appendix=1
    \hb@xt@\hsize{\hfil\MakeUppercase{\appendixname}\hfil}
    \hb@xt@\hsize{\hfil#1\hfil}
   \else
    \ifnum \c@secnumdepth >\m@ne
     {\sbox\@tempboxa{\thechapter\ \ {#1}}
      \ifdim \wd\@tempboxa >\hsize
        \begin{center}
          \unhbox\@tempboxa
        \end{center}
      \else
        \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}
      \fi}
    \else
      \hb@xt@\hsize{\hfil#1\hfil}
    \fi
   \fi
   \par\nobreak
   {\singlespace
   \vspace*{3\baselineskip}}}}
\def\@schapter#1{\if@twocolumn
                   \@topnewpage[\@makeschapterhead{\MakeUppercase{#1}}]%
                 \else
                   \@makeschapterhead{\MakeUppercase{#1}}%
                   \@afterheading
                 \fi}
\def\@makeschapterhead#1{%
  \ifcase\c@appendix
    \setlength{\@tempdima}{\@chapterheadskip}%
    \or\setlength{\@tempdima}{\@chapterheadskip}%
    \or\setlength{\@tempdima}{\@appendicesheadskip}%
    \else\@ctrerr
  \fi
  \addtolength{\@tempdima}{-\headsep}%
  \addtolength{\@tempdima}{-\@topmargin}%
  \vspace*{\@tempdima}%
  {\parindent \z@ \raggedright \normalfont
   \interlinepenalty\@M
   {\sbox\@tempboxa{#1}
    \ifdim \wd\@tempboxa >\hsize
      \begin{center}
        \unhbox\@tempboxa
      \end{center}
    \else
      \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}
    \fi}
   \par\nobreak
   {\singlespace
   \vspace*{3\baselineskip}}}}
\renewcommand\section{\@startsection{section}{1}{\z@}%
                                   {3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                     {3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont}}
\if@titlepage
  \renewenvironment{abstract}
    {{\chapter*{\MakeUppercase{\abstractname}
      \@mkboth{\MakeUppercase{\abstractname}}{\MakeUppercase{\abstractname}}}%
      \addcontentsline{toc}{chapter}{\MakeUppercase{\abstractname}}
      \singlespace

      \noindent
      \@lastnamefirst.  \@degreebrief, Purdue University, \@date.
      \@title. Major Professor: \@advisor.

      \doublespace
      \par}}
    {\par\vfil\newpage}
\fi
\newenvironment{dedication}
  {\newpage\null\vfil\vfil\begin{center}}
  {\end{center}\vfil\vfil}
\renewenvironment{glossary}
  {\separatorpage{\MakeUppercase{\glossaryname}}
   \chapter*{\MakeUppercase{\glossaryname}
      \@mkboth{\MakeUppercase{\glossaryname}}{\MakeUppercase{\glossaryname}}}
   \addcontentsline{toc}{chapter}{\MakeUppercase{\glossaryname}}}
  {\par\vfil\newpage}
\newenvironment{vita}
  {\separatorpage{\MakeUppercase{\vitaname}}
   {\chapter*{\MakeUppercase{\vitaname}
    \@mkboth{\MakeUppercase{\vitaname}}%
    {\MakeUppercase{\vitaname}}}
   \addcontentsline{toc}{chapter}{\MakeUppercase{\vitaname}}}}
  {\par\vfil\newpage}
\newenvironment{acknowledgments}
  {{\chapter*{\MakeUppercase{\acknowledgmentsname}
    \@mkboth{\MakeUppercase{\acknowledgmentsname}}%
    {\MakeUppercase{\acknowledgmentsname}}}}}
  {\par\vfil\newpage}
\def\@begintheorem#1#2{\trivlist \item[\hskip \labelsep{#1\ #2}]}
\def\@opargbegintheorem#1#2#3{\trivlist
      \item[\hskip \labelsep{\bf #1\ #2\ (#3)}]}
\renewenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \setcounter{page}{1}
      \thispagestyle{empty}}
    {%
      \if@restonecol
        \twocolumn
      \else
        \newpage
      \fi}
\def\@toccolumn{}
\newcommand\@toc[2]{%
   {\def\@toccolumn{#2}%
    \@tocpageline
    \@starttoc{#1}%
    \newpage}}
\renewcommand\tableofcontents{%
  \chapter*{{\MakeUppercase{\contentsname}}%
    \@mkboth{\MakeUppercase{\contentsname}}{\MakeUppercase{\contentsnamee}}}%
  \@toc{toc}{}}
\newcounter{appendix}
\setcounter{appendix}{0}
\renewcommand\appendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \setcounter{appendix}{2}%
  \addcontentsline{toc}{chapter}{\MakeUppercase{\appendicesname}}
  \separatorpage{\MakeUppercase{\appendicesname}}%
  \renewcommand\@chapapp{\appendixname}%
  \renewcommand\thechapter{\@Alph\c@chapter}}
\newcommand\oneappendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \setcounter{appendix}{1}%
  \separatorpage{\MakeUppercase{\appendixname}}%
  \renewcommand\@chapapp{\appendixname}%
  \renewcommand\thechapter{\@Alph\c@chapter}}
\def\endappendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \setcounter{appendix}{0}%
  \renewcommand\@chapapp{\chaptername}%
  \renewcommand\thechapter{\@Arabic\c@chapter}}
\renewcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    \@tocpagebreak
    \addpenalty{-\@highpenalty}%
    \if@pageline
      \@pagelinefalse
    \else
      \vskip 1.0em \@plus\p@
    \fi
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak
      \leaders\hbox{$\m@th
        \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
        mu$}\hfill
      \nobreak
      \hb@xt@\@pnumwidth{\hss #2}\par
      \penalty\@highpenalty
    \endgroup
  \fi}
\renewcommand*\l@part[2]{%
  \ifnum \c@tocdepth >-2\relax
    \@tocpagebreak
    \addpenalty{-\@highpenalty}%
    \if@pageline
      \@pagelinefalse
    \else
      \addvspace{2.25em \@plus\p@}%
    \fi
    \begingroup
      \setlength\@tempdima{3em}%
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      {\leavevmode
       #1\nobreak
       \leaders\hbox{$\m@th
         \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
         mu$}\hfill
       \nobreak
       \hb@xt@\@pnumwidth{\hss #2}}\par
       \nobreak
       \global\@nobreaktrue
       \everypar{\global\@nobreakfalse\everypar{}}%
    \endgroup
  \fi}
\renewcommand*\l@section{%
  \ifnum 1>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{1}{1.5em}{2.3em}}
\renewcommand*\l@subsection{%
  \ifnum 2>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{2}{3.8em}{3.2em}}
\renewcommand*\l@subsubsection{%
  \ifnum 3>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{3}{7.0em}{4.1em}}
\renewcommand*\l@paragraph{%
  \ifnum 4>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{4}{10em}{5em}}
\renewcommand*\l@subparagraph{%
  \ifnum 5>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{5}{12em}{6em}}
\renewcommand*\l@figure{%
  \ifnum 1>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{1}{0em}{2.3em}}
\renewcommand*\l@table{%
  \ifnum 1>\c@tocdepth \else
    \@tocpagebreak
    \@pagelinefalse
  \fi
  \@dottedtocline{1}{0em}{2.3em}}
\newdimen\@tocheight
\setlength{\@tocheight}{\textheight}
\addtolength{\@tocheight}{-\twoskip\baselineskip}
\newif\if@pageline
\@pagelinefalse
\newcommand\@tocpagebreak{%
  \ifdim \pagetotal>\@tocheight
    \clearpage
    \@tocpageline
  \fi}
\newcommand\@tocpageline{%
    \hb@xt@\hsize{\@toccolumn\hfil Page}
    \vskip 1.0em \@plus\p@
    \@pagelinetrue}
\renewcommand\listoffigures{%
  \chapter*{{\MakeUppercase{\listfigurename}}%
    \@mkboth{\MakeUppercase{\listfigurename}}{\MakeUppercase{\listfigurename}}}%
  \addcontentsline{toc}{chapter}{\MakeUppercase{\listfigurename}}
  \@toc{lof}{Figure}}
\renewcommand\listoftables{%
  \chapter*{{\MakeUppercase{\listtablename}}%
    \@mkboth{\MakeUppercase{\listtablename}}{\MakeUppercase{\listtablename}}}%
  \addcontentsline{toc}{chapter}{\MakeUppercase{\listtablename}}
  \@toc{lot}{Table}}
\newcommand{\separatorpage}[1]{
  \newpage
  \thispagestyle{empty}
  \addtocounter{page}{-1}
  \null
  \vfil\vfil
  \begin{center}
    {\normalfont\rmfamily #1}
  \end{center}
  \vfil\vfil
  \newpage
}
\renewenvironment{thebibliography}[1]
   {\separatorpage{\MakeUppercase{\bibname}}
    \chapter*{\MakeUppercase{\bibname}
      \@mkboth{\MakeUppercase{\bibname}}{\MakeUppercase{\bibname}}}
    \addcontentsline{toc}{chapter}{\MakeUppercase{\bibname}}
    \singlespace
    \list{\@biblabel{\@arabic\c@enumiv}}%
         {\settowidth\labelwidth{\@biblabel{#1}}%
          \leftmargin\labelwidth
          \advance\leftmargin\labelsep
          \advance\leftmargin\@bibindent
  \itemindent -\@bibindent
  \listparindent \itemindent
          \@openbib@code
          \usecounter{enumiv}%
          \let\p@enumiv\@empty
          \renewcommand\theenumiv{\@arabic\c@enumiv}}%
    \sloppy
    \clubpenalty4000
    \@clubpenalty \clubpenalty
    \widowpenalty4000%
    \sfcode`\.\@m}
  {\def\@noitemerr
    {\@latex@warning{Empty `thebibliography' environment}}%
    \endlist}
\renewenvironment{theindex}
   {\if@twocolumn
      \@restonecolfalse
    \else
      \@restonecoltrue
    \fi
    \columnseprule \z@
    \columnsep 35\p@
    \twocolumn[\@makeschapterhead{\MakeUppercase{\indexname}}]%
    \@mkboth{\MakeUppercase{\indexname}}%
            {\MakeUppercase{\indexname}}%
    \thispagestyle{plain}\parindent\z@
    \parskip\z@ \@plus .3\p@\relax
    \let\item\@idxitem}
  {\if@restonecol\onecolumn\else\clearpage\fi}
\renewcommand\contentsname{Table of Contents}
\renewcommand\listfigurename{List of Figures}
\renewcommand\listtablename{List of Tables}
\newcommand\refname{References}
\renewcommand\bibname{Bibliography}
\renewcommand\indexname{Index}
\renewcommand\figurename{Figure}
\renewcommand\tablename{Table}
\renewcommand\partname{Part}
\renewcommand\chaptername{Chapter}
\renewcommand\appendixname{Appendix}
\renewcommand\abstractname{Abstract}
\newcommand\appendicesname{Appendices}
\newcommand\acknowledgmentsname{Acknowledgments}
\newcommand\vitaname{Vita}
\newcommand\glossaryname{Glossary}
\newcounter{currenthour}
\newcounter{currentminute}
\newcommand{\now}{
  \setcounter{currenthour}{\time}\divide \c@currenthour by 60
  \setcounter{currentminute}{\time}
    \multiply \c@currenthour by 60
    \addtocounter{currentminute}{-\c@currenthour}
    \divide \c@currenthour by 60
  \ifnum \c@currenthour=0 0\fi    % force at least one digit in hour
  \thecurrenthour:%
  \ifnum \c@currentminute<10 0\fi % force two digits in minute
  \thecurrentminute}
\if@twoside
\else
  \raggedbottom
\fi
\widowpenalty=10000
\brokenpenalty=10000
\clubpenalty=10000
\ps@thesis
\pagenumbering{arabic}
\endinput
%%
%% End of file `thesis.cls'.

--3/zvnxgQN0
Content-Type: text/plain
Content-Description: Thesis bibliography style
Content-Disposition: inline;
	filename="thesisbib.sty"
Content-Transfer-Encoding: 7bit

% From acmbib.sty - modified to not \footnotesize the bibliography,
%                   which messes up the Purdue thesis format style.
%
% Bibliographic cite forms needed:
%
%  \cite{key}
%    which produces citations with author list and year.
%    eg. [Brown 1978; Jarke, et al. 1985]
%  \citeA{key}
%    which produces citations with only the author list.
%    eg. [Brown; Jarke, et al.]
%  \citeN{key}
%    which produces citations with the author list and year, but
%    can be used as nouns in a sentence; no brackets appear around
%    the author names, but only around the year.
%      eg. Shneiderman [1978] states that......
%    \citeN should only be used for a single citation.
%    \citeNN{refkey1,refkey2} for author [ref1year; ref2year]
%    \citeyear{key}
%        which produces the year information only, within brackets.
%
% Abbreviated author lists use the ``et al.'' construct.
%
% The above are examples of required ACM bibliographic cite formats needed.
% *******************
% Here is the complete list of cite forms from the chicago bibliographic style
%
%  \cite{key}
%    which produces citations with abbreviated author list and year.
%  \citeNP{key}
%    which produces citations with abbreviated author list and year.
%  \citeA{key}
%    which produces only the abbreviated author list.
%  \citeANP{key}
%    which produces only the abbreviated author list.
%  \citeN{key}
%    which produces the abbreviated author list and year, with only the
%    year in parentheses. Use with only one citation.
%  \citeyear{key}
%    which produces the year information only, within parentheses.
%  \citeyearNP{key}
%    which produces the year information only.
%
% Abbreviated author lists use the ``et al.'' construct.
%
% `NP' means `no parentheses' 
%
%
%-----------------------BIBLIOGRAPHY STUFF-------------------------
% this is adapted (November 1993) by Andrew Appel and Rebecca Davies from
% 
%%%     filename        = "chicago.sty",
%%%     version         = "4",  % MODIFIED!
%%%     date            = "31 August 1992",
%%%     time            = "09:42:44 199",
%%%     author          = "Glenn Paulley",
%%%     address         = "Data Structuring Group
%%%                        Department of Computer Science
%%%                        University of Waterloo
%%%                        Waterloo, Ontario, Canada
%%%                        N2L 3G1",
%%%     telephone       = "(519) 885-1211",
%%%     FAX             = "(519) 885-1208",
%%%     email           = "gnpaulle@bluebox.uwaterloo.ca",
%%%
%%% ====================================================================
%
% this file: Modification of chicago.sty for new ACM bibliography
% style, which is similar (but not identical) to the ``Chicago'' style.
%
% chicago.sty: Style file for use with bibtex style chicago.bst, for
% bibliographies formatted according to the 13th Edition of the Chicago
% Manual of Style.
%
% 'newapa.bst' was made from 'plain.bst', 'named.bst', and 'apalike.bst',
% with lots of tweaking to make it look like APA style, along with tips
% from Young Ryu and Brian Reiser's modifications of 'apalike.bst'.
% newapa.sty formed the basis of this style, chicago.sty. Author-date
% references in newapa.bst formed the basis for chicago.bst. Chicagoa.bst
% supports annotations.
%
% Version 4 (August, 1992):
% - fixed chicago.bst and chicagoa.bst to handle long author lists in
%   sorting
% - fixed chicago.bst and chicagoa.bst so that missing page numbers in
%   ``article'' entries are handled correctly
% - modified chicago.sty to format entries with 2nd and subsequent lines
%   indented.
%
%   Citation format: (author-last-name year)
%             (author-last-name and author-last-name year)
%             (author-last-name et al. year)
%             (author-last-name)
%             author-last-name
%             author-last-name (year)
%             (author-last-name and author-last-name)
%             (author-last-name et al.)
%             (year) or (year,year)
%             year or year,year
%
%   Reference list ordering: alphabetical by author or whatever passes
%    for author in the absence of one.
%
% This BibTeX style has support for abbreviated author lists and for
%    year-only citations.  This is done by having the citations
%    actually look like
%
%    \citeauthoryear{full-author-info}{abbrev-author-info}{year}
%
% The LaTeX style has to have the following (or similar)
%
%     \let\@internalcite\cite
%     \def\fullcite{\def\citeauthoryear##1##2##3{##1, ##3}\@internalcite}
%     \def\fullciteA{\def\citeauthoryear##1##2##3{##1}\@internalcite}
%     \def\shortcite{\def\citeauthoryear##1##2##3{##2, ##3}\@internalcite}
%     \def\shortciteA{\def\citeauthoryear##1##2##3{##2}\@internalcite}
%     \def\citeyear{\def\citeauthoryear##1##2##3{##3}\@internalcite}
%
%
% -------------------------------------------------------------------------
%
% Citation macros.
%
%    \begin{macrocode}
\let\@internalcite\cite
\def\cite{\def\@citeseppen{-1000}%
    \def\@cite##1##2{[##1\if@tempswa , ##2\fi]}%
    \def\citeauthoryear##1##2##3{##2 ##3}\@internalcite}
\def\citeNP{\def\@citeseppen{-1000}%
    \def\@cite##1##2{##1\if@tempswa , ##2\fi}%
    \def\citeauthoryear##1##2##3{##2 ##3}\@internalcite}
\def\citeN{\def\@citeseppen{-1000}%
    \def\@cite##1##2{##1\if@tempswa , ##2]\else{]}\fi}%
    \def\citeauthoryear##1##2##3{##2 [##3}\@citedata}
\def\citeNN{\def\@citeseppen{-1000}%
    \def\@cite##1##2{[##1\if@tempswa , ##2\fi]}%
    \def\citeauthoryear##1##2##3{##3}\@citedata}
\def\citeA{\def\@citeseppen{-1000}%
    \def\@cite##1##2{[##1\if@tempswa , ##2\fi]}%
    \def\citeauthoryear##1##2##3{##2}\@internalcite}
\def\citeANP{\def\@citeseppen{-1000}%
    \def\@cite##1##2{##1\if@tempswa , ##2\fi}%
    \def\citeauthoryear##1##2##3{##2}\@internalcite}
\def\citeyear{\def\@citeseppen{-1000}%
    \def\@cite##1##2{[##1\if@tempswa , ##2\fi]}%
    \def\citeauthoryear##1##2##3{##3}\@citedata}
\def\citeyearNP{\def\@citeseppen{-1000}%
    \def\@cite##1##2{##1\if@tempswa , ##2\fi}%
    \def\citeauthoryear##1##2##3{##3}\@citedata}
%    \end{macrocode}
%
% \@citedata and \@citedatax:
%
% Place commas in-between citations in the same \citeyear, \citeyearNP,
% or \citeN command.
% Use something like \citeN{ref1,ref2,ref3} and \citeN{ref4} for a list.
%
%    \begin{macrocode}
\def\@citedata{%
	\@ifnextchar [{\@tempswatrue\@citedatax}%
				  {\@tempswafalse\@citedatax[]}%
}
%    \end{macrocode}
%
%    \begin{macrocode}
\def\@citedatax[#1]#2{%
\if@filesw\immediate\write\@auxout{\string\citation{#2}}\fi%
  \def\@citea{}\@cite{\@for\@citeb:=#2\do%
    {\@citea\def\@citea{; }\@ifundefined% by Young
       {b@\@citeb}{{\bf ?}%
       \@warning{Citation `\@citeb' on page \thepage \space undefined}}%
{\csname b@\@citeb\endcsname}}}{#1}}%
%    \end{macrocode}
%
% don't box citations, separate with ; and a space
% also, make the penalty between citations negative: a good place to break.
%
%    \begin{macrocode}
\def\@citex[#1]#2{%
\if@filesw\immediate\write\@auxout{\string\citation{#2}}\fi%
  \def\@citea{}\@cite{\@for\@citeb:=#2\do%
    {\@citea\def\@citea{; }\@ifundefined% by Young
       {b@\@citeb}{{\bf ?}%
       \@warning{Citation `\@citeb' on page \thepage \space undefined}}%
{\csname b@\@citeb\endcsname}}}{#1}}%
%    \end{macrocode}
%
% No labels in the bibliography.
%
%    \begin{macrocode}
\def\@biblabel#1{}
%    \end{macrocode}
%
% Set length of hanging indentation for bibliography entries.
%
%    \begin{macrocode}
\newlength{\bibhang}
\setlength{\bibhang}{2em}
%    \end{macrocode}
%
% Alternate way of using citeyear
%    \begin{macrocode}
\def\shortcite#1{\citeyear{#1}}
%
% Fix cite so it doesn't repeat author lists in citations:
%
%    \begin{macrocode}
\def\cite{\def\@citeseppen{-1000}%
    \def\@cite##1##2{[##1\if@tempswa , ##2\fi]}%
    \let\@lastauthor=\@noauthor
    \let\citeauthoryear=\citeauthoryear@no@repeats\@internalcite}
    %\def\citeauthoryear##1##2##3{##2 ##3}\@internalcite
\def\@noauthor={\relax}
\let\@lastauthor=\@noauthor
\let\@currauthor=\@noauthor
\def\citeauthoryear@no@repeats#1#2#3{%
  \def\@currauthor{\csname @author #1\endcsname}%
  \ifx\@lastauthor\@currauthor{#3}\else{#2 #3}\fi%
 \let\@lastauthor=\@currauthor}
%    \end{macrocode}
%
%    \begin{macrocode}
\newdimen{\@bibindent}
\setlength\@bibindent{1.5em}
%    \end{macrocode}

--3/zvnxgQN0
Content-Type: text/plain; charset=us-ascii
Content-Description: .signature
Content-Transfer-Encoding: 7bit



-- 
******************************************************************************
David Whitlock                                          whitlock@cs.purdue.edu
Purdue University                     http://www.cs.purdue.edu/homes/whitlock/
******************************************************************************

--3/zvnxgQN0--

