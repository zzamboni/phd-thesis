######################################################################
# Files of the dissertation

CHAPTER_FILES =  $(wildcard ch*.tex) app.tex vita.tex biblio.tex
CHAPTERS = $(basename $(CHAPTER_FILES))
CHAPTER_FLAGS = $(addprefix .flag_,$(CHAPTERS))
CHAPTERS_PS = $(addsuffix .ps,$(CHAPTERS))

######################################################################
# Commands

MV = mv
RM = rm -f

# Find prv at ftp://ftp.dante.de/pub/tex/support/prv/
# Installed under /usr/local/prv in CERIAS
# It automatically runs latex, bibtex etc as necessary. If you don't
# have it, set LATEX = latex and see the lines below marked with >>>>

# We use the -ig option (ignore missing dependencies) because it
# does not know how to deal with included graphics that are not
# given the full extension, so it marks them as missing.
LATEX = prv -ig

# Flag to control the generation of a tech report cover page.
# Has to be "no" or "yes"
TECHREP = no

######################################################################
# Main targets

# When calling any of these, you can set TECHREP=yes to produce the
# document with a Tech Report cover page.

thesis: tables_check thesis.dvi $(FORCE) done
dvi: tables_check thesis.dvi $(FORCE) done
ps: tables_check thesis.ps $(FORCE) done
pdf: tables_check thesis.pdf $(FORCE) done

psf:
	make ps FORCE=yes LATEXFLAGS=-go

psr:
	make ps LATEXFLAGS=-scan_force FORCE=yes

rescan:
	make LATEXFLAGS=-scan_force FORCE=yes

# Build all the chapters, each in its own file
all:
	make $(CHAPTERS) MOVE=yes FORCE=$(FORCE)

# Check that the automatically generated tables are up to date.
tables_check:
	cd tables; make tables

# Other targets: chX (X = chap #), clean, realclean, bak

# Normally the chapter targets (ch1, ch2 ...) leave the document
# in thesis.dvi. If the MOVE variable has a non-empty value, the dvi
# file is renamed to the corresponding chX.dvi file (see the all:
# target)

######################################################################
# General rules

.SUFFIXES: .tex .dvi .ps

.tex.dvi:
	$(LATEX) $(LATEXFLAGS) ${<}
# >>>> If you don't have prv, set LATEX=latex and uncomment the following:
#	while grep "^$(LATEX) Warning:.*Rerun" ${*}.log ;\
#	do \
#		$(LATEX) ${<};\
#	done

.dvi.ps:
	$(LATEX) -ps ${*}
#	dvips -o ${*}.ps ${<}

######################################################################

# IONLY contains the includeonly construct. This file is generated
# by this makefile and should be empty when it is planned to
# process the whole thesis.
IONLY = includeonly.tex

# The file 'tr-include.tex' is either empty (in which case the standard
# dissertation is produced) or includes the Tech Report title page.
TRINC = tr-include.tex

######################################################################
# Specific files

thesis.dvi: thesis.tex thesis.bbl puthesis.cls $(IONLY) tr-$(TECHREP) $(CHAPTER_FILES) yes
	$(LATEX) $(LATEXFLAGS) thesis
# >>>> If you don't have prv, set LATEX=latex and uncomment the following:
#	while grep "^$(LATEX) Warning:.*Rerun" thesis.log ;\
#	do \
#		$(LATEX) thesis;\
#	done

thesis.pdf: thesis.tex thesis.bbl puthesis.cls $(IONLY) tr-$(TECHREP) $(CHAPTER_FILES) yes
	$(LATEX) -pdf $(LATEXFLAGS) thesis

thesis.tex: ${IONLY} tr-${TECHREP}

thesis.bbl: thesis.tex biblio.tex
# >>>> If you don't have prv, set LATEX=latex and uncomment the following:
#	$(LATEX) thesis.tex
#	-bibtex thesis

$(IONLY):
	touch ${IONLY}

tr-no:
	rm -f $(TRINC)
	touch $(TRINC)

tr-yes:
	rm -f $(TRINC)
	echo "\\input{tr-titlepage}" >> $(TRINC)

.flag_%: %.tex $(FORCE)
	echo "\\includeonly{"$(basename $<)"}" >> $(IONLY)
	touch $@

$(CHAPTERS): %:.flag_% $(FORCE)
	make thesis FORCE=$(FORCE)
	if [ ! -z "$(MOVE)" ]; then mv thesis.dvi $@.dvi; fi

$(CHAPTERS_PS): $(FORCE)
	make $(basename $@) MOVE=yes FORCE=$(FORCE)
	dvips -o $@ $(basename $@)

clean:
	rm -f *~ *.log *.bak \#* *.au[xk] *.bbl *.blg $(CHAPTER_FLAGS) *.to[ck] *.dep *.lof *.lot *.idx *.ind *.ilg *.cb *.lod $(IONLY) $(TRINC)
	cd tables; make clean

realclean: clean
	rm -f thesis.ps thesis.dvi thesis.pdf

# Backup my thesis directory everywhere
bak:
	ssh trinity-2 'cd Thesis; cvs upd -Pd'

######################################################################
# Other miscellaneous targets

f:
	make FORCE=yes

get:
	scp dzamboni@expert.cc.purdue.edu:ids_db/ids.txt data
	scp dzamboni@expert.cc.purdue.edu:WWW/ids/enter_ids.cgi tools/cgi

yes:

done:
	@echo ""
