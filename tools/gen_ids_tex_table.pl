eval 'exec perl -x $0 ${1+"$@"}' # -*-perl-*-
  if 0;
#!perl -w
#
use strict;
use File::Copy;

######################################################################
# Configuration

my $template=shift(@ARGV) ||
  "$ENV{HOME}/Thesis/Dissertation/tables/classification_ids_template.tex";
my $file=shift(@ARGV) || "$ENV{HOME}/Thesis/Dissertation/data/ids.txt";
#my $outfile=shift(@ARGV) ||
#  "$ENV{HOME}/Thesis/Dissertation/tables/classification_ids.tex";

######################################################################
# Global variables

my %db=();

# Subroutines to add attributes according to flags.
my %flagsubs=(
	      Commercial => sub { shift },
	      Government => sub { shift },
	      Research   => sub { shift },
	      Freeware   => sub { shift },
	      Recheck    => sub { shift },
	     );

# Flags and order in which they appear
my @flags=qw(Research Commercial Government Freeware Recheck);

# Map field abbreviations to full names
my %fields=(
	    das => "Data analysis structure",
	    dcs => "Data collection structure",
	    dcm => "Data collection mechanism",
	    com => "Comments",
	    flg => "Flags",
	    url => "URLs",
	    bib => "Bibliographic entries",
	   );
# Order in which fields should be displayed
my @fields=qw(das dcs dcm com flg url bib);

# Values for some of the fields.
my %fieldvals=(
	       das => [qw(Centralized Distributed)],
	       dcs => [qw(Centralized Distributed)],
	       dcm => [qw(Indirect/NetBased Indirect/HostBased
			  Direct/External Direct/Internal)],
	       flg => [@flags],
	      );

######################################################################

read_db();

open(IN, "<$template")
  or die "Error opening $template: $!\n";
# open(OUT, ">$outfile")
#   or die "Error opening $outfile: $!\n";

while (<IN>) {
    if (/--idss\(([^,]*),([^,]*),([^,]+),([^,]*)\)/) {
	my @params=($1,$2,$3,$4);
	foreach (@params) {
	    $_=undef if (!$_ || $_ eq 'undef' || $_ eq 'any');
	}
	print join(", ", map { format_entry($_) } get_idss(@params))."\n";
    }
    else {
	s/--pct\(([a-z]+),([^\)]+)\)/&do_pct($1,$2)/ge;
	print;
    }
}
close IN;
#close OUT;

######################################################################

# Format a name for printing
sub format_entry {
    my $n=shift;
    my $t="";
    # Add the bibliographic reference if it has one.
    if ($db{$n}->{bib} &&
        $db{$n}->{bib} =~ /^\s*\@[a-zA-Z]+\{\s*(.*),\s*$/m) {
      $t="~\\citep{$1}";
    }
    $n=~s/\&/\\&/;
    return "$n$t";
}

######################################################################

# Return the IDSs that satisfy the conditions given.
sub get_idss {
  my ($das, $dcs, $dcm, $flg, $includeESP)=@_;
  my @res=();
  foreach my $n (sort keys %db) {
    push @res, $n
      if ( (!$das || $das eq $db{$n}->{das}) &&
	   (!$dcs || $dcs eq $db{$n}->{dcs}) &&
	   (!$dcm || $db{$n}->{dcm} =~ /$dcm/) &&
	   (!$flg || $db{$n}->{flg} =~ /$flg/) &&
	   ($includeESP || ($n ne 'ESP'))   # don't include ESP in the result
                                            # unless requested.
	 );
  }
  return @res;
}

######################################################################

# Return the percentage of systems that have certain values on
# a certain category.
sub do_pct {
    my %fpos=('das' => 0, 'dcs' => 1, 'dcm' => 2, 'flg' => 3);
    my ($f,$v)=@_;
    my @v=split ',', $v;
    my %res;
    foreach (@v) {
	my @params=();
	$params[$fpos{$f}]=$_;
        $params[4]=1;  # include ESP in the count
	@res{get_idss(@params)}=();
    }
    return sprintf("%.0f", 100*scalar(keys %res)/scalar(keys %db));
}

######################################################################

# Read database in memory, store it in %db.

# The format of the file is:
# name,das,dcs,dcm,flg,com
# URLs:
# <urls>
# endURLs
# BIBs:
# <bibs>
# endBIBs
#
# Where:
#   das - Data Analysis Structure
#   dcs - Data Collection Structure
#   dcm - Data Collection Mechanisms
#   flg - Flags
#   com - Comments
# The URLs and BIBs sections are optional.
#
# We really should use XML... :-)
#
# The format of %db is:
# %db -> {name} -> {das}, {dcs}, {dcm}, {flg}, {com}, {url}, {bib}
sub read_db {
  my @lines=();			# temporary line buffer
  my $getline=sub { my $l=(shift(@lines) || <F>); $l };
  my $putback=sub { unshift @lines, @_ };

  if (open F, "<$file") {
    while ($_=&$getline) {
      chomp;
      my ($n,$das,$dcs,$dcm,$flg,$com)=split(/,/, $_, 6);
      next unless $n =~ /\S/;
      $db{$n}={dcs => $dcs,	# data collection structure
	       das => $das,	# data analysis structure
	       dcm => $dcm,	# data collection mechanism
	       com => $com,	# comment
	       flg => $flg||"Research", # flags
	      };
      my $tmp=&$getline;
      my $url="";
      if (defined($tmp) && $tmp=~/^URLs:/) {
	while ($_=&$getline) {
	  last if /^endURLs/;
	  $url.=$_;
	}
      } else {
	$putback->($tmp);
      }
      $tmp=&$getline;
      my $bib="";
      if (defined($tmp) && $tmp=~/^BIBs:/) {
	while ($_=&$getline) {
	  last if /^endBIBs/;
	  $bib.=$_;
	}
      } else {
	$putback->($tmp);
      }
      $url=~s/^\s+//; $url=~s/\s+$//;
      $bib=~s/^\s+//; $bib=~s/\s+$//;
      $db{$n}->{url}=$url;
      $db{$n}->{bib}=$bib;
    }
    close F;
  }
}

######################################################################

# Write to the file
sub write_db {
  # Make a backup copy first
  copy($file, "${file}.bak");
  open F, ">${file}.new"
    or die "Error opening ${file}.new for writing: $!\n";
  foreach my $k (sort keys %db) {
    next unless $k;
    # Remove DOS EOLs that Netscape inserts in text fields. Ugh.
    $db{$k}->{$_} =~ tr /\r//d foreach (keys %{$db{$k}});
    print F "$k,";
    print F join(",", @{$db{$k}}{qw(das dcs dcm flg com)})."\n";
    print F "URLs:\n".$db{$k}->{url}."\nendURLs\n" if $db{$k}->{url};
    print F "BIBs:\n".$db{$k}->{bib}."\nendBIBs\n" if $db{$k}->{bib};
  }
  close F;
  rename "${file}.new", $file
    or die "Error renaming ${file}.new to $file: $!\n";
}

