
\chapter{RELATED WORK AND ARCHITECTURES FOR \ID}
\label{chap:id-architectures}

\Id is conceptually---and in practice, in most cases---performed in
two distinct phases: data collection and data analysis. \Idss can be
classified according to how they are structured in each of those
phases.

In this chapter, we describe the different structures that an \ids can
have in both data collection and data analysis and draw some
conclusions that guide the rest of this dissertation. We also mention
related work as appropriate. Tables~\ref{tab:classification_ids}
and~\ref{tab:classification_ids_das-dcs} summarize how different
existing \idss are classified according to some of the architectures
described in this chapter.

\input{tables/classification_ids_das-dcs}
\input{tables/classification_ids}

%----------------------------------------------------------------------

\section{Data collection architectures}
\label{sec:datacollection}

The performance of an \ids can only be as good in terms of desirable
characteristics \#\ref{dc:fault}, \#\ref{dc:subversion}, \#\ref{dc:adapt},
and \#\ref{dc:detect} (see
Section~\ref{sec:desirable-characteristics-ids}) as the data on which
it bases its decisions. For this reason, the way in which data is
obtained is an important design decision in the development of \idss.
If the data is acquired with a significant delay, detection could be
performed too late to be useful.  If the data is incomplete, detection
abilities could be degraded.  If the data is incorrect (because of
error or actions of an intruder), the \ids could stop detecting
certain intrusions and give its users a false sense of security.
Unfortunately, these problems have been identified in existing
products. After examining the needs of different \idss and the data
provided by different operating systems, \citeauthor{price97:msthesis}
concluded that ``the audit data supplied by conventional operating
systems lack content useful for misuse
detection.''~\citep[p.~107]{price97:msthesis}

With the goal of better understanding the characteristics that make
data collection mechanisms suitable for \id, in this chapter we
provide two conceptual (centralized/distributed and direct/indirect)
and two practical (host/network-based and external/internal)
classifications of data collection mechanisms. We discuss the
advantages and disadvantages of each one of them.

%I
%also discuss our experiences with the different types of data
%collection in the design and implementation of the
%AAFID~\citep{spafford00:intrus_detec_auton_agent} distributed \ids.

The term \emph{monitored component} is used in this chapter and in the 
rest of this dissertation as follows:
\begin{wdefinition}{Monitored component}
  A host or a program that is being monitored by an \ids.
\end{wdefinition}

By this definition, an \ids that is explicitly monitoring several
programs in a host (for example, the kernel, the email server and the
HTTP server) could be considered as monitoring several components even
if they are all in the same host.


\subsection{Data collection structure: centralized and distributed}
\label{sec:centralized-distributed-datacollection}

When talking about data collection architectures for \id, the
classification normally refers to the locus of data collection.
\CB{The following definitions are based on those provided by
  \citet{axelsson98:resear_intrus_detec_system}.}

\begin{definition}{Centralized data collection}
  Data used by the \ids is collected at a number of locations that is
  fixed and independent of the number of monitored components.
\end{definition}
\begin{definition}{Distributed data collection}
  Data used by the \ids is collected at a number of locations that is
  directly proportional to the number of monitored components.
\end{definition}

\CB{In these definitions, a \emph{location} is defined as an instance
  of running code. So for example, a data collection mechanism
  implemented in a shared library could be considered as distributed
  provided that the library will be linked against multiple programs,
  because each running program will execute the mechanism separately.
  However, if the shared library will be linked against a single
  program and it will collect all the information needed by the \ids,
  we would consider it as centralized data collection. We can see that
  these definitions depend not only on how the data collection
  components are implemented, but also on how they are used.}

As can be seen in Table~\ref{tab:classification_ids_das-dcs}, both
distributed and centralized data collection have been widely used in
existing \idss and have almost equal percentages in the systems listed
in the table.  A report by
\citet{axelsson98:resear_intrus_detec_system} shows that the trend
over the years has been towards distributed \idss, which need
distributed data collection.

The distinction between centralized and distributed data collection is
the feature most commonly used for describing the data collection
capabilities of an \ids. However, for the purposes of this
dissertation, we are more interested in discussing the mechanisms used
to perform data collection and the data sources utilized. These are
described in the next sections.

\subsection{Data collection mechanisms: direct and indirect monitoring}
\label{sec:direct-indirect}

\CB{
  
  In the physical world, a direct observation is one in which we can
  use one or more of our senses to observe or measure a phenomenon,
  and an indirect observation is one in which we rely on a tool or on
  an observation by someone (or something) else to obtain the
  information.
  
  We build similar definitions in the context of data collection for
  \id. When an \ids can measure a condition or observe behavior in the
  monitored component by obtaining data directly from it, we use the
  term \emph{direct monitoring}. When the \ids relies on a separate
  mechanism or tool for obtaining the information, we use the term
  \emph{indirect monitoring}. In other words, direct monitoring is the
  measurement or observation of a characteristic of an object, and
  indirect monitoring is the measurement or observation of the effects
  of the object having that characteristic.
  
  For example, using the \cmd{ps}~\citep{man:ps} command to observe
  CPU load on a Unix host is considered a case of direct monitoring
  because \cmd{ps} directly extracts the load data from the
  corresponding data structures in the kernel. By comparison, if the
  CPU load is recorded in a log file and later read from there, we
  consider it a case of indirect monitoring because we are relying on
  a separate mechanism (in this case, a file) for the observation.
  
  Based on the above discussion, we state that all data collection
  methods can be classified as direct or indirect according to the
  following definitions:

  \begin{definition}{Direct monitoring}
    The observation of the monitored component by obtaining data
    directly from it.
  \end{definition}

  \begin{definition}{Indirect monitoring}\label{def:indirect-monitoring}
    The observation of the monitored component through a separate
    mechanism or tool.
  \end{definition}
  
  Common examples of mechanisms through which indirect monitoring can
  be performed are log files and network packets. The data obtained
  from these mechanisms is an effect of the data having been present
  in the components that generated the data. If the data were obtained
  directly from the component that generated them (for example, by
  reading the appropriate data structures on the host before a packet
  is sent to the network), we would be performing direct monitoring.

}

To perform \id, direct monitoring is better than indirect monitoring
for the following reasons:

\begin{description}
\item[Reliability:] Data from an indirect data source (for example, a
  log file) could potentially be altered by an intruder before the
  \ids uses it. It could also be affected by non-malicious failures.
  \CB{For example, a disk becoming full or a log file being renamed
    could make the data unavailable to the \ids.}

\item[Completeness:] Some events may not be recorded on an indirect
  data source. For example, not every action of the \cmd{inetd} daemon
  gets recorded to a log file.
  
  Furthermore, an indirect data source may not be able to reflect
  internal information of the object being monitored. For example, a
  TCP-Wrappers~\citep{venema:tcp-wrapper} log file cannot reflect the
  internal operations of the \cmd{inetd} daemon. It can only contain
  data that is visible through its external interfaces. \CB{While that
    information may be sufficient for some purposes (for example,
    knowing what address a request came from), it may not be
    sufficient for others (for example, knowing which specific access
    rule caused a request to be denied).}

\item[Volume:] With indirect monitoring, the data is generated by
  mechanisms (for example, the code that writes the audit trail) that
  have no knowledge of the needs of the \ids that will be using the
  data. For this reason, indirect data sources usually carry a high
  volume of data. For example,
  \mbox{\citet{kumar95:softw_archit_misus_intrus_detec}} mention that
  a C2-generated audit trail might contain 50K-500K records per user
  per day. For a modest-size user community, this could amount to
  hundreds of megabytes of audit data per day, as pointed out
  by~\citet{mounji97:phd_thesis}.
  
  For this reason, when indirect data sources are used, the \ids has
  to spend more resources in filtering and reducing the data even
  before being able to use them for detection purposes.
  
  A direct monitoring method has the ability to select and obtain only
  the information it needs. As a result, smaller amounts of data are
  generated. Additionally, the monitoring components could partially
  analyze the data themselves and only produce results when relevant
  events are detected. This would practically eliminate the need for
  storing data other than for forensic purposes.
  
\item[Scalability:] The larger volume of data generated by indirect
  monitoring results in a lack of scalability. As the number of hosts
  and monitoring elements grows, the overhead resulting from filtering
  data can cause degradation in the performance of the hosts being
  monitored or overload of the network on a centralized \ids.
  
\item[Timeliness:] Indirect data sources usually introduce a delay
  between the moment the data is produced and when the \ids can have
  access to them.  Direct monitoring allows for shorter delays and
  enables the \ids to react in a more timely fashion.
\end{description}

However, as can be seen in Table~\ref{tab:classification_ids}, there
is a notable disparity in the utilization of direct and indirect
monitoring in \idss. Less than 20\% of the \idss surveyed use some
form of direct monitoring. This can be attributed to the main
disadvantage of direct monitoring: complexity of implementation.
Direct monitoring mechanisms have to be designed in a more specific
manner to the monitored component and the type of information that it
generates. Evidence to this is that most of the \idss in
Table~\ref{tab:classification_ids} (except for
CylantSecure~\citep{wimer01:cylantsecure} and
pH~\citep{somayaji00:autom_respon_using_system_call_delay}) that use
direct monitoring are tailored for detecting specific types of
attacks.

\subsection{Data collection mechanisms: host-based and network-based}
\label{sec:host-network}

In practice, data collection methods are commonly classified as
host-based or network-based according to the following definitions:
\begin{definition}{Host-based data collection}
  The acquisition of data from a source that resides on a host, such
  as a log file, the state of the system or the contents of memory.
\end{definition}
\begin{definition}{Network-based data collection}
  The acquisition of data from the network. Usually done by capturing
  packets as they flow through it.
\end{definition}

Most of the intrusions detected by \idss are caused by actions
performed in a host. For example: executing an invalid command or
accessing a service and providing it malformed or invalid data.  The
attacks act on the end host although they may occur over a network.

Also, there is the case of attacks that act on the network
infrastructure components such as routers and switches. Most of those
components can be considered as hosts, and they have the ability to
perform monitoring tasks on
themselves~\citep{bradley98:_detec_disrup_router}. Therefore, attacks
on the network infrastructure can also be considered as acting on
hosts. In cases where the network infrastructure components cannot
perform monitoring tasks (for example, because they are not
programmable), attacks on those components can only be detected using
network-based data collection because the attacks do not act directly
on any other hosts in the network. \CB{For the rest of our discussion,
  we will consider routers and switches as hosts.}

The only attacks that act on the network itself are those that flood
the network to its capacity and prevent legitimate packets from
flowing. However, most of these attacks can also be detected at the
end hosts. For example, a Smurf attack~\citep{huegen00:smurfing} could
be detected at the ICMP layer in the host by looking for the
occurrence of a large number of ECHO\_RESPONSE packets.

%The only case in which network-based data collection could be better
%suited than host-based data collection is for attacks that flood the
%network with packets that do not cause any reaction on the hosts (for
%example, packets destined to a port that is closed on all
%hosts).

In general, it is advisable to use host-based data collection for the
following reasons (see also the discussion
by~\citet{daniels99:ip-vuln}):

\begin{itemize}
  
\item Host-based data collection allows collecting data that
  reflect accurately what is happening on the host, instead of trying
  to deduce it based on the packets that flow through the network.
  
\item In high-traffic networks, a network monitor could potentially
  miss packets, whereas properly implemented host monitors can report
  every single event that occurs on each host.
  
\item Network-based data collection mechanisms are subject to
  insertion and evasion attacks, as documented
  by~\citet{ptacek.newsham:insertion}. These problems do not occur on
  host-based data collection because they act on data that the host
  already has.
  
\item If the data needed by the \ids flows through disjoint paths (as
  might be the case with a network with multiple gateways or when
  using switching hubs), performing network-based data collection can
  become difficult and unreliable, and the task of unifying the data
  coming from different collectors for use by the \ids may not be
  trivial.
  
\item The use of encryption renders network-based data collection
  mechanisms ineffective because they cannot examine the contents of
  encrypted communications.
  
\item Network-based data collection mechanisms cannot observe actions
  that occur inside a host, so they will miss local attacks.

\end{itemize}

Network-based data collection also has some advantages, including the
following:

\begin{itemize}

\item An \ids that uses network-based data collection can be deployed
  on an existing network without having to make any changes to the
  hosts. For this reason, a large number of commercial \idss use
  network-based data collection.
  
\item A network-based data collection component can be completely
  invisible to other hosts (this can be achieved even at the hardware
  level), providing a convenient vantage point from which to observe
  the actions on the network.

\end{itemize}

We consider network-based data collection as a form of indirect
monitoring because the network traffic \CB{is an effect of the data
  and activity at the hosts (see
  Definition~\ref{def:indirect-monitoring}).}  In a more general
sense, the advantages and disadvantages just described reflect the
distinction between direct and indirect data collection.

The relationship between the traditional host-based/network-based
classification of \idss~\citep{ID:MuHeLe94} and the types of
monitoring described in Section~\ref{sec:direct-indirect} is as
follows and can be seen in Table~\ref{tab:classification_ids}: \Idss
normally considered as ``network-based'' correspond to
Indirect/Network-based monitoring mechanisms, whereas
Indirect/Host-based and all Direct monitoring mechanisms correspond to 
the ``host-based'' \idss.

Both host-based and network-based data collection have been widely
used in \idss. In recent years, an increasing number of \idss have
started to use both host-based and network-based components in an
attempt to obtain the most complete view of the hosts being monitored.

The architecture described in this dissertation corresponds to a
system that uses host-based data collection.

\subsection{Data collection mechanisms: external and internal sensors}
\label{sec:external-internal}

All direct monitoring methods are host-based. Direct monitoring of a
host can be done using external or internal sensors according to the
following definitions:

\begin{definition}{External sensor}
  A piece of software \CB{that observes a component (hardware or
    software) in a host and reports data usable by an \ids, and that
    is implemented by code separate from that component.}
\end{definition}
\begin{definition}{Internal sensor}
  A piece of software \CB{that observes a component (hardware or
    software) in a host and reports data usable by an \ids, and that
    is implemented by code incorporated into that component.}
\end{definition}

For example, a program that uses the \cmd{ps} command~\citep{man:ps}
to obtain process information on a Unix system could be considered an
external sensor. If the process-information gathering component was
built into the Unix kernel, it would be considered an internal sensor.
A library wrapper~\citep{kuperman98:library-interposition} is
considered as an external sensor \CB{because its code is separate from
  that of the program it monitors. According to our definitions, an
  internal sensor could also be built into hardware components; for
  example, in the firmware of a network interface card.}

Internal sensors are part of the source code of the monitored
component. They can be added to an already existing program, and in
that case they can be considered as a case of source code
instrumentation. Ideally, internal sensors should be added during
development of the program when the cost and effort of making changes
and fixing errors is lower~\citep{peng93:softw_error_analy}. Also, at
that point the sensors could be added by the original authors of the
program instead of by someone else---who would have the added cost of
understanding the program first.

\CB{Note that by our definitions, any portion of a program can be
  considered as an internal sensor, as long as it provides data that
  can be used by an \ids. No specification is made about how the data
  should be produced or transmitted}.

\input{tables/adv-disadv-external}
\input{tables/adv-disadv-internal}

External and internal sensors for direct data collection have
different strengths and weaknesses and can be used together in an
\ids. Tables~\ref{tab:adv-disadv-external}
and~\ref{tab:adv-disadv-internal} list the advantages and
disadvantages of each type of sensor.

From the point of view of software engineering, internal and external
sensors present different characteristics in the following aspects:

\begin{description}
\item[Introduction of errors:] It is potentially easier to introduce
  errors in the operation of a program through the use of internal
  sensors because the code of the program being monitored has to be
  modified. Errors can also be introduced by external sensors (for
  example, an agent that consumes an excessive amount of resources, or
  an interposed library call that incorrectly modifies its arguments).
  We claim that most internal sensors can be fairly small pieces of
  code. Their size allows them to be extensively checked for errors.
  Also, this problem would be reduced if sensors were added during
  development of the program instead of afterwards.
  
\item[Maintenance:] External sensors are easier to maintain
  independently of the program they monitor because they are not part
  of it. However, when internal changes to the program occur, it can
  be simpler to update internal sensors (which can be changed at the
  same time the program is modified) than external sensors (which have 
  to be kept up to date separately).
  
\item[Size:] Internal sensors can be smaller (in terms of code size
  and memory usage) than external sensors because they become part of
  an existing program; thus, avoiding the base overhead associated with
  the creation of a separate process.
  
\item[Completeness:] Internal sensors can access any piece of
  information in the program they are monitoring whereas external
  sensors are limited to externally-available data. For this reason,
  internal sensors can have more complete information about the
  behavior of the monitored program.  Furthermore, because internal
  sensors can be placed anywhere in the program they are monitoring,
  their coverage can be more complete than that of an external sensor
  which can only look at the program ``from the outside.''
  
\item[Correctness:] Because internal sensors have access to more
  complete data, we expect them to produce more correct results than
  external sensors, which often have to act based on incomplete data.

\end{description}

External sensors are better in terms of ease of use and
maintainability whereas internal sensors are superior in terms of
monitoring and detection abilities, resilience and host impact. Both
types of sensors can be used in an \ids to take advantage of their
strengths according to the specific task each sensor has to
accomplish.

We can see in Table~\ref{tab:classification_ids} that a small
percentage of the \idss surveyed use internal sensors and most of
those were designed for detecting specific types of attacks---\CB{the
  two exceptions are CylantSecure~\citep{wimer01:cylantsecure} which
  uses internal sensors for collecting information analyzed
  externally, and
  pH~\citep{somayaji00:autom_respon_using_system_call_delay}, which
  fully implements data collection and detection using internal
  sensors, and is a good example of the potential of internal
  sensors.}  This can be attributed to the considerable difficulty in
the implementation of internal sensors: the monitored components
themselves have to be modified. On closed-source systems, this is
impossible unless the vendor provides the modifications, and on
open-source systems it can be cumbersome and time consuming.

%Table~\ref{tab:classification_ids} includes ESP, the \ids implemented
%based on the architecture described in this dissertation. As can be
%seen in the table, it is the only \ids (apart from
%FormatGuard~\citep{cowan01:formatguard}, which is a very specific tool
%for detecting format-string buffer overflows) that uses distributed
%data collection, distributed data analysis, and internal sensors, and
%that is designed for detecting a wide variety of intrusions.


\section{Data analysis architectures}
\label{sec:analysis-architectures}

\Idss are classified as centralized or distributed with respect to how
the data analysis components are distributed, as
follows~\citep{spafford00:intrus_detec_auton_agent}:

\begin{definition}{Centralized \ids}
  An \ids in which the analysis of the data is performed in a number
  of locations that is fixed and independent of the number of
  monitored components.
\end{definition}
\begin{definition}{Distributed \ids}
  An \ids in which the analysis of the data is performed in a number
  of locations that is directly proportional to the number of
  monitored components.
\end{definition}

Note that these definitions are based on the number of monitored
components and not of hosts (as has been traditionally the case), so
it is feasible to have an \ids that uses distributed data analysis
within a single host if the analysis is performed in different
components of the system.

\CB{In the definitions above, a \emph{location} is defined as an
  instance of running code. So for example, an analysis component
  implemented in a shared library could be considered as a distributed
  analysis component if the library will be linked against multiple
  programs, because each running program will execute the analysis
  component separately. However, if the shared library will be linked
  against a single program and all the data analysis will occur there,
  we would consider it as centralized analysis. So we can see that
  these definitions depend not only on how the analysis components are
  implemented, but also on how they are used.}

Both distributed and centralized \idss may use host- or network-based
data collection methods, or a combination of them.

Some strengths and weaknesses of centralized and distributed \idss are
shown in Table~\ref{tab:centralized-distributed}.

%\afterpage{\clearpage\input{tables/centralized-distributed}}
\input{tables/centralized-distributed}

It can be observed from Table~\ref{tab:classification_ids_das-dcs}
that the vast majority of \idss surveyed use centralized data
analysis. This can be attributed to the difficulty in the
implementation of a distributed analysis mechanism.

Most weaknesses of distributed \idss can be overcome through technical
means whereas centralized \idss have some fundamental limitations
(for example, with respect to scalability and graceful degradation of
service). In the last few years, an increasing number of distributed
\idss has been designed and
built~\citep[e.g.][]{hofmeyr:99,spafford00:intrus_detec_auton_agent,bradley98:_detec_disrup_router,porras.neumann:emerald,staniford-chen.cheung.al:grids}.


\input{building-aafid}


\section{Comments about \id architectures}
\label{sec:comments-architectures}

In this chapter, we have described some of the main architectural
concepts that are used in \id. In data collection, host-based direct
monitoring using internal sensors presents multiple benefits in terms
of efficiency, reliability and data collection abilities.

\CB{However direct monitoring using internal sensors has been used by
  few \idss, as shown in Table~\ref{tab:classification_ids}.}  This is
a consequence of the implementation difficulty of internal sensors and
of the lack of studies regarding their properties and the related
design and implementation issues. \CB{Also, internal sensors lack
  portability and increase the cost of deployment and maintenance of
  the \idss because they require dealing with the source code of each
  program that needs to be monitored. However, the general idea of
  using internal sensors is to get as close as possible to the data
  sources needed by the \ids, and are the only mechanism able to
  provide other desirable characteristics, including fidelity,
  reliability and resistance to attacks. For these reasons, it is
  important to explore their capabilities and limitations.}

In data analysis, a distributed architecture provides multiple
benefits in terms of scalability, reliability and efficiency. This is
the reason why through the years, \ids research and development has
tended towards working on distributed
systems~\citep{axelsson98:resear_intrus_detec_system}.

Note that distributed \idss are usually associated with operation on
multiple hosts. However, according to the definitions given in this
chapter, the components of a distributed \ids do not necessarily have
to be in different hosts. If multiple parts of a single host are being
monitored, and the data analysis components reside on different parts
of the system, they could be considered as a single-host distributed
\ids.

This chapter is organized around the distinction between the data
collection and data analysis steps. Conceptually, this distinction is
useful for analysis and for reasoning about the \id process. Its
usefulness has been shown in efforts to model the \id
process~\citep{axelsson98:resear_intrus_detec_system} and
\idss~\citep{porras:cidf_architecture}.

In practice, essentially every \ids has followed this separation by
making data collection and analysis two distinct steps separated in
time and often in space. However, this separation has the following
shortcomings:
\begin{itemize}
\item It creates a window of time between the generation and the use
  of data. This can cause inconsistencies between what the \ids
  ``sees'' and the state of the system at the time the data is
  analyzed. It also increases the possibility that the data get
  modified before the \ids analyzes them, either by accident or
  malicious action. Furthermore, it reduces the timeliness of the
  reactions of the \ids: by the time it analyzes the data and reacts
  to an intrusion, it may be too late to do anything about it.
\item It lengthens the path through which the data has to flow between
  its generation and its use. This increases the amount of traffic in
  the system (within the host or over the network), reducing the
  scalability of the \ids. It also increases the time between the
  generation and use of the data and brings along all the problems
  described in the previous item.
\end{itemize}

For these reasons, in practice, the data collection and analysis steps
should be as close together as possible.

The rest of this dissertation describes a distributed architecture
based on internal sensors that addresses many of the problems
mentioned above and has significant beneficial properties with respect
to the desirable characteristics described in
Section~\ref{sec:desirable-characteristics-ids}.


\section{Related work}
\label{sec:related-work}

The lack of adequate audit data for \id was documented
by~\citet{price97:msthesis}, showing that most \idss in existence
today operate with incomplete data, which are insufficient to support
adequate detection. Internal sensors are able to overcome limitations
in auditing systems by performing direct monitoring and completely
skipping the operating system's auditing system.

The work by~\citet{ID:Crosbie95b,AI-ID:Crosbie95} provided the
foundation for using a large number of small independent components in
\id. This work also provides an idea of how internal sensors could
become more complex entities when necessary. They could even learn or
evolve as they capture data about their environment.

\CB{The analysis of system call sequences to detect intrusions
  proposed by \citet{forrest.hofmeyr.al:self} is a technique that
  lends itself naturally to be implemented using internal sensors.
  This was demonstrated in practice by the further development of the
  pH system based on that
  technique~\citep{somayaji00:autom_respon_using_system_call_delay},
  which is implemented almost completely inside the Linux kernel. The
  pH system also responds to attacks by slowing down or aborting
  system calls, showing the potential that internal sensors have for
  providing not only detection but also response capabilities.}

The collection of data using specialized mechanisms for detecting
certain vulnerabilities in a Unix kernel was described
by~\citet{daniels99:ip-vuln}. That work focused on low-level IP
vulnerabilities, and described the generation of new audit events
(which could be classified as internal sensors) and the implementation
of methods for detecting certain vulnerabilities.

\citet{ErlSch99} described the use of \emph{reference monitors} to
monitor the execution of a program. The reference monitors they
describe are implemented as code that evaluates a security automaton
and is inserted automatically before any instruction that accesses
memory.  These reference monitors could be considered as internal
sensors that check for generic violations of policy. The monitors also
halt the program when a violation is detected, so it can be considered
as a reactive \ids.

The concept of application-level \id has been described
by~\citet{Sielken99}, who discussed advantages of the approach from a
theoretical standpoint. We agree with the advantages that
application-specific monitoring can provide for \id. Internal sensors
are an ideal tool for this purpose because they can be embedded into
any program, whether it is a system program or a user-level
application.

The idea of using library interposition for \id, as described
by~\citet{kuperman98:library-interposition} was a first step in doing
direct data collection for \id. We classify it as a form of external
sensors, but we think it can be further developed to provide good
application-specific \id by, for example, tailoring interposed
libraries to specific applications, or combining data generated by
interposed libraries with data provided by internal sensors to get a
complete picture of what is happening in a program.

As shown in Table~\ref{tab:classification_ids}, few \idss have been
developed using internal sensors.
CylantSecure~\citep{wimer01:cylantsecure} utilizes internal sensors
but only in the form of counters whose values are used to build a
profile of program behavior. The values reported by the sensors are
analyzed and profiled by an external program. The
LIDS~\citep{huagang00:lids} and Openwall~\citep{linux-openwall-patch}
projects have developed kernel patches for Linux~\citep{Beck:1996:LKI}
that prevent certain operations defined as ``dangerous.'' These
patches add checks that constitute internal sensors, but are
specifically tuned for preventing those operations.

Another example of the use of internal sensors is
FormatGuard~\citep{cowan01:formatguard}. This is a specialized tool
for detecting and preventing format-string-based buffer
overflows~\citep{scut01:exploit_format_strin_vulner,
  newsham00:format_strin_attac}.  By recompiling the affected
programs, code is inserted for checking when a format string attack is
attempted against any of the functions instrumented. These pieces of
code constitute internal sensors that detect attacks in a distributed
fashion (because even within a single host, the ``data analysis'' is
done by the sensors at each monitored component). Format string
attacks are difficult to detect, and FormatGuard is one clear example
of one of the advantages of internal sensors over external sensors:
They can access internal information of the monitored component and
can even add or re-implement functionality or information as needed to
aid in the detection.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "~/Thesis/Dissertation/thesis"
%%% End: 
