
\subsection{Embedded detectors for sendmail attacks}
\label{sec:sendmaildetectors}

Sendmail~\citep{ora-sendmail-2ed} is the most widely used
mail-delivery agent on Unix machines. A number of security problems
have been encountered in sendmail over the years, and many of them can
still be found in systems connected to
networks~\citep{cisco:vulner_statis_repor}.

Sendmail is a complex user-level process with multiple clearly
identifiable vulnerabilities in its past. For this reason, it was an
ideal candidate for the implementation of detectors outside the
kernel.


\subsubsection{Detectors implemented}

We implemented the detectors in version 8.10.1 of sendmail which is
the version included with OpenBSD 2.7. During the initial test phase,
11 sendmail detectors were implemented, and they are summarized in
Table~\ref{tab:sendmail-detectors}.
\input{tables/sendmail-detectors}

In the next sections we will describe in more detail some of these
detectors. As with the network detectors described in
Section~\ref{sec:networkdetectors}, the sendmail detectors are
surrounded by \textbf{\#ifdef} statements that allow to disable or
enable them individually at compile time. No runtime mechanism exists
for disabling or enabling these detectors.

\subsubsection{Stateless detectors}

Seven of the 11 sendmail detectors implemented in this phase were
stateless. Most of the vulnerabilities to which these detectors
correspond have been fixed in the newer versions of sendmail. In some
cases the new code specifically looks for and avoids the corresponding
attacks. In those cases, the detectors consisted of simple checks or
only the calls to the reporting mechanism. This is the case for most
of the detectors that consist of only one or two executable
statements.

As an example, we present the detector for CVE-1999-0096 corresponding
to the use of the ``decode'' alias to overwrite arbitrary files on a
system. This alias is no longer enabled by default in new versions of
sendmail, but because there are still old versions of sendmail in use
on the Internet, it is important to detect attempts to use those
aliases. In this case, the detector specifically looks for mail sent
to the \texttt{decode} address or the equivalent \texttt{uudecode}
address:
\pagebreak
\begin{code}
\ldots
a->q_next = al;
a->q_alias = ctladdr;
\textbf{#ifdef ESP_CVE_1999_0096
\{ if (a != NULL && a->q_user != NULL) \{
    if((strcmp(a->q_user,"decode")==0)|| 
       (strcmp(a->q_user,"uudecode")==0))\{
     esp_logf("CVE-1999-0096: name='%s'\n", a->q_user);
    \}
  \}
\}
#endif}
 \ldots
\end{code}
Note that this detector works even if the addresses it looks for do
not exist on the system and shows one of the advantages of embedded
detectors: they can look for attempts to exploit vulnerabilities that
do not exist on the host being monitored.


\subsubsection{Stateful detectors}

Stateful detectors are more complex than stateless ones. In the
simplest cases, the detector has to collect some piece of information
at an early stage before being able to make a decision later on. For
example, the detector for CVE-1999-0130 needs two pieces of
information to determine that an attack is occurring: the sendmail
program needs to be run under the name \prog{smtpd} and the user that
invoked it must not be \acct{root}. Because these two pieces of
information are available at different points in the program, the
detector is split in two code segments. The first one sets a flag when
sendmail is being run as \prog{smtpd}:
\begin{code}
\textbf{#ifdef ESP_CVE_1999_0130
  bool esp_RunAsSmtpd = FALSE;
#endif}
\ldots
else if (strcmp(p, "smtpd") == 0) \{
  OpMode = MD_DAEMON;
\textbf{#ifdef ESP_CVE_1999_0130
  esp_RunAsSmtpd = TRUE;
#endif}
\}
\end{code}
The second code segment is executed in the same block in which
sendmail already generates an error message when ``daemon mode'' is
requested by a non-root user, and generates the corresponding alert:

\begin{code}
 usrerr("Permission denied");
\textbf{#ifdef ESP_CVE_1999_0130
 if (esp_RunAsSmtpd) \{
  esp_logf("CVE-1999-0130: user=%d\n", RealUid);
 \}
#endif}
 finis(FALSE, EX_USAGE);
\end{code}

A more complex example of a stateful detector is the one for
CVE-1999-0047, which detects attempts to exploit a buffer overflow in
the MIME-decoding subroutine of sendmail 8.8.3/8.8.4. This detector is 
interesting because it illustrates how in some cases it is difficult
to differentiate between normal and intrusive behavior.

Under normal circumstances, the \fnc{mime7to8()} function of sendmail
uses a fixed-length buffer that gets repeatedly filled and flushed as
necessary while decoding a MIME message. In the vulnerable versions, a
typo in the code (checking the wrong variable to see if the buffer was
already full) prevented the buffer from being flushed, allowing the
program to keep writing past the end of the buffer and causing the
buffer overflow.

Once the problem was fixed, the buffer is correctly flushed every time
it fills. However, it is impossible in the fixed code to detect an
attack against this vulnerability by looking at the behavior of the
program because both regular and attack data behave exactly the same:
they fill the buffer, which gets flushed, and the process repeats as
many times as necessary.

Therefore, to build this detector we resorted to heuristics. In this
particular case, we look at the data that are being written into the
buffer and compare them against the data used by the most common
exploit script that was circulated for this vulnerability. This is
done in the function \fnc{esp\_mime\_buffer\_overflow()}:
\begin{code}
\textbf{#ifdef ESP_CVE_1999_0047
char
esp_mime_buffer_overflow(char c, int filled, char *msg) \{
 char egg[]=
   "\\xeb\\x37\\x5e
      \emph{(more binary data omitted)}"
 static int pos=0;
 static int count=0;
 if (esp_match_char(egg, c, &pos, &count, 0x00, 0) && filled) \{
   esp_logf("%s\n", msg);
   pos=0;
 \}
 return c;
\}
#endif}
\end{code}
This subroutine does a character-by-character matching against the
binary ``egg'' used by the exploit script and returns success when a
complete match is found. In a more complex version of the detector, a
fuzzy or partial match could be done, or the search could look for
more than one binary string in the data.

From the \fnc{mime7to8()} function, the
\fnc{esp\_\-mime\_\-buffer\_\-overflow()} function is called every time
a character is inserted in the decoding buffer:
\begin{code}
  *fbufp = (c1 << 2) | ((c2 & 0x30) >> 4);
\textbf{#ifdef ESP_CVE_1999_0047
  esp_mime_buffer_overflow(*fbufp, esp_filled, "CVE-1999-0047");
#endif}
  \ldots
\end{code}

An additional heuristic used to signal an attack is that the decoding
buffer must have been filled and flushed at least once when the binary
string is encountered (otherwise a buffer overflow would not have
occurred in the vulnerable code), so the detector also keeps track of
how many times the buffer has been filled:
\begin{code}
  \ldots
  putxline((char *) fbuf, fbufp - fbuf, mci, PXLF_MAPFROM);
  fbufp = fbuf;
\textbf{#ifdef ESP_CVE_1999_0047
  esp_filled++;
#endif}
\}
\end{code}

This detector keeps track of several pieces of information available
only inside the sendmail code, which shows the advantage that embedded
detectors have by being able to access internal information of the
program. This detector also shows one of the drawbacks of the embedded
detectors approach: when the vulnerability for which the detector is
built no longer exists in the code, it can be difficult to
differentiate between normal behavior of the program and behavior
under attack. This problem is common to all existing signature-based
\idss.


\subsubsection{Testing the detectors}

Each detector was tested using the exploit scripts available for each
vulnerability. In most cases the exploit scripts were available from
the same sources in which the problem was described, but in others we
had to develop our own exploits. Each detector correctly signaled the
attacks when they were launched using the exploit scripts.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% TeX-master: "main"
%%% End: 
