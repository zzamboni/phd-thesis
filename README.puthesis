Using the LaTeX thesis format.

The Purdue latex thesis format style is a set of macros designed to
make it easy if not trivial to slip your thesis past the little old
lady that approves the format of your thesis.  The thesis formatting
rules are described at length in the document  XXXX.  The thesis
macros make it easy to format your dissertation; however, the macros
can't help with content.

Currently, the last updated macros are in
/homes/mb/TeX/ThesisStyle.  To use them, add /homes/mb/TeX/ThesisStyle
to your TEXINPUTS path. The Makefile actually already does that.  
Look at the definition of LATEX at the beginneng of the Makefile.

At present, the latex macros are only 98 % acceptable to the thesis
office.  There are still several places where the resultant output
does not meet official guidelines.  However, if one smiles and agrees
to fix the problem right away, you will probably be allowed to
graduate. (I did!)

Known problems:

1) Long chapter titles are not split properly.  The thesis format
guide says that the chapter heading should be centered.  If the title
must be split across multiple lines, each line should be centered.
Currently, the latex package left justifies extra lines.  (Easy fix:
don't have long chapter titles!)

2) If you have figures or tables in the appendix, you must add special
lines to the lists of figures and tables.  See the12.sty for how to do
this - the code is there, just commented out.

The easiest way to get started is to copy this skeleton directory into
your home directory and start editing the contents of the files.  Each
chapter is given its own subdirectory.  This structure facilitates
working on one chapter at a time.  Normally, one edits a chapter,
prints a copy on the laser printer, and repeats the cycle.

To keep things simple, I suggest that you only run latex from the top
level directory.  That is, if you want to run latex on chapter 4, go
to the top level directory and issue: "make chap4".  Assuming that all
goes well, this will create chap4.dvi, which you can preview with xdvi
or send to a laser printer.

The actual meat of the dissertation resides in subdirectories, one for
each chapter.  

Subdirectory ch0:

Chapter 0 contains the abstract and table of contents that precede the
dissertation.  The file abstract.tex contains the abstract, and
ch0.tex contains miscellaneous other things, most of which are pretty
obvious.

Subdirectories ch1-ch3

These directories contain the meat of your dissertation.  There is
nothing magical about 6 chapters;  add more as you see fit, but
remember to update the Makefile.

Each chapter starts with a:

     \chapter{heading}

where heading is the name of your chapter.  Sections are started by
the command:

    \section{this is a section}

You can also have subsections (e.g., with \subsection{name}).

Figures are best generated using the following construct (or something
similar):

\begin{figure}[tbp]
{\small
\input{ch3/fig1.tex}
\centerline{\box\graph}
}
\vspace*{10mm}
\caption{Forwarding entities that cooperate to transport messages from
Purdue to SGI}
\label{cypress.fig}
\end{figure}

The figure is kept in the file "ch3/fig1.tex" (recall that you are
running latex from the top level directory, not from within ch3).  The
"vspace" construct is needed to add enough space between the figure
and the caption below it (Purdue format once again).  The rest of the
commands are described in the latex book.

BIBLIOGRAPHY

Your bibliography database goes in the top level directory (e.g.,
thesis.bib).  See the latex book for the correct format.  Latex will
automatically generate your bibliography (assuming you use the
skeleton file biblio/biblio.tex)

VITA

