%----------------------------------------------------------------
%  ch5.tex -- this is here just to conform to the structure
%             which the Makefile expects. 
%----------------------------------------------------------------

\chapter {TESTING THE ESP IMPLEMENTATION}
\label{chap:experimental-results}

After the initial ESP implementation was completed, a series of tests
was performed to measure its responses and to obtain qualitative and
quantitative results about its behavior. The tests were designed to
evaluate the performance impact of the ESP \ids on an instrumented
host and its detection abilities for previously unknown attacks.

%----------------------------------------------------------------------

\section{Performance testing}
\label{sec:performance-tests}


\subsection{Test design and methodology}
\label{sec:performance-test-design}

The purpose of the performance tests was to determine the impact that
the ESP sensors and detectors have on the instrumented host, under
severe but non-intrusive operating conditions (the detectors were not
triggered during these tests). For this purpose, we decided to focus
on two groups of detectors:
\begin{enumerate}
\item Detectors in the networking portions of the kernel (24 detectors).
\item Detectors in a web servers (32 detectors).
\end{enumerate}
These are the two largest groups of detectors (see
Figure~\ref{fig:by_impl-dir}) and are good representatives of
detectors in the kernel and in a user space application respectively.
The detectors are additional code to be executed. Because they do not
interfere with their surrounding code, their main impact is in terms
of additional execution time. We measured CPU utilization and compared
systems compiled with and without the detectors.

\input{figures/fig_tests-setup} The general setup for the tests was as
shown in Figure~\ref{fig:tests-setup}: One server B, that would be the
one instrumented with the detectors when appropriate, and where the
CPU utilization would be measured; and a client A, from which the
tests would be launched against B. These two machines were on a
dedicated point-to-point network connected with a third machine R
operating as a transparent bridge between A and B. The purpose of R
was to allow the artificial reduction of the bandwidth available for
the connection between A and B. Hosts A and B were 600 MHz Intel
Pentium III machines with 128 MB RAM running OpenBSD 2.7, and host R
was a 700MHz Intel Celeron machine with 128 MB RAM running
FreeBSD~\citep{FreeBSD-website} and
\prog{dummy\_net}~\citep{rizzo-dummynet} for imposing constraints on
the bandwidth. \CB{Unnecessary programs and services were stopped on
  the test machines (including the \prog{syslog} and \prog{cron}
  daemons, and the X Windows system) to reduce the factors that could
  confound the performance measurements.}

The first test was done using a subset of the
NetPerf~\citep{netperf-website} benchmark. The NetPerf test we used
measures network performance as the maximum throughput between two
hosts by sending a stream of data from a source to a sink over a TCP
or UDP connection.  We selected the TCP version of the test because 16
of the 24 detectors implemented in the networking sections are in the
IP or TCP layers.  In this test, the independent variable was the
maximum bandwidth allowed between the source (A) and the sink (B) and
was controlled by setting bandwidth constraints on R using
\prog{dummy\_net}. We measured CPU utilization on B under increasing
bandwidth, from 5~Mbps up to 100~Mbps.

In the second test, a web server was running on host B while host A
was generating requests. The web server used was
Apache~\citep{apache-website} as included in the OpenBSD 2.7
distribution. We used \prog{http\_load}~\citep{http_load-website}
to generate the requests by randomly choosing URLs from a list. The
independent variable in this test was the number of simultaneous
connections that \prog{http\_load} was allowed to establish. We
measured CPU utilization on host B under an increasing number of
simultaneous connections, from 5 up to 100.

For each value of the independent variable, twenty runs of the test
were performed. All the runs were duplicated in two blocks: one for
host B with detectors (ESP block) and one without detectors (NOESP
block).  Each run lasted for 60 seconds and during that period,
snapshot observations of the CPU load in host B were taken each
second. The CPU load was obtained using the \cmd{top}~\citep{man:top}
command, which uses information gathered in the \fnc{statclock()}
function within the kernel context
switch~\citep[][p.~58]{McKusick:1996:DIO}. Three observations at the
beginning and the end of each run were ignored (to eliminate ramp-up
and ramp-down measurements), and the rest (54 observations) were
averaged to obtain an average CPU load for each run.

All the information for each test is summarized in
Table~\ref{tab:test-summaries}. The order in which the independent
variable was modified for the NetPerf and http\_load tests was
generated using a pseudo-random number generator with a fixed seed to
be able to reproduce the sequence.
\input{tables/test-summaries}

%The sequence for each one of the
%experiments is listed in Tables~\ref{tab:order-netperf}
%and~\ref{tab:order-http_load} respectively.

Before each block all the systems were rebooted and a ``warm-up''
sequence was run by applying all the values of the independent
variable in increasing order. This was done to bring the hosts to a
stable state in terms of caching, disk spinning and any possible
unknown factors, before taking any measurements.

Although we attempted to arrange the experimental setup to minimize
extraneous effects on the measurements, there are still factors that
could affect them, including virtual memory, process scheduling and
caching. The measurement process itself runs on the CPU being
measured, which may affect the observations as well. Finally, as
mentioned, the results reported consist of an average of averages,
which may compound errors in the measurements.

However, the purpose of these experiments was to compare the behavior
of hosts with and without detectors, and not to establish absolute
measurements of performance. As such, this setup and methodology is
adequate for showing the impact that embedded detectors have on the
host in which they reside.



\subsection{Results of the NetPerf test}
\label{sec:results-netperf}

%The CPU measurements obtained in host B during the execution of the
%NetPerf experiment are listed in Table~\ref{tab:numbers-netperf}.
%\input{tables/numbers-netperf}

Figure~\ref{fig:scatter-means-netperf} shows the CPU measurements
obtained in host B during the execution of the NetPerf experiment.
There are 20 points at each value of $X$ for each block (ESP and
NOESP) and the lines connect the mean values at each value of $X$.
\input{figures/scatter-means-netperf} We can see in this graph that
for lower values of $X$, the CPU utilization is essentially the same,
but the difference grows larger as $X$ increases, because the
detectors in the networking layers of the kernel introduce additional
work that needs to be done for every packet that is received. To
quantify the difference, a pair-wise F-test was done for each value of
$X$, and its results are shown in Table~\ref{tab:sas-results-netperf}.
\input{tables/sas-results-netperf} The p-values in this table show
that the difference in means between ESP and NOESP can be considered
statistically non-significant up to about $X=20$, but after that point
it is statistically significant. Figure~\ref{fig:means-diff-netperf}
shows the difference between the means at each point, with its 95\%
confidence interval.   \input{figures/means-diff-netperf}

The results of this experiment are what would be expected, with the
detectors having a larger impact as the amount of work that the system
does increases. The detectors can have a considerable impact on the
CPU load of the host, particularly for high values of $X$. However, we
should keep in mind that this test was specifically designed to stress
the host by maintaining a constant stream of the appropriate bandwidth
fed to it. Under normal operating conditions, the average network load
being processed by a host is lower; therefore the impact of the
detectors should not be as noticeable. Furthermore, although the
difference is \emph{statistically} significant, it is never more than
7\% of CPU utilization, which in practical terms could be considered
acceptable. \CB{At its maximum value (for $X=85$), the difference in
  means is 6.6\%. The NetPerf test is exercising a maximum of 24
  detectors (those implemented in the networking layers of the
  kernel), so on average each detector adds less than 0.3\% to the CPU
  load of the system. In reality, not all detectors have the same
  impact (because of their functionality, implementation, and where
  they are placed), but this number is an indication of the small
  impact that each individual detector has.}

\subsection{Results of the http\_load test}
\label{sec:results-http_load}

%The CPU measurements obtained in host B during the execution of the
%http\_load experiment are listed in Table~\ref{tab:numbers-http_load},
%\input{tables/numbers-http_load}

The CPU measurements obtained in host B during the execution of the
http\_load experiment are shown in
Figure~\ref{fig:scatter-means-http_load}, with
the mean values plotted as lines.
\input{figures/scatter-means-http_load}
The results from a pair-wise F-test are shown in
Table~\ref{tab:sas-results-http_load}, and
Figure~\ref{fig:means-diff-http_load} shows the difference between the 
means with their 95\% confidence interval.
\input{tables/sas-results-http_load}
\input{figures/means-diff-http_load}

In this case, the results are indicative of the detectors having a
large impact on the CPU utilization of the host. Counter intuitively,
we can see that the CPU utilization on the system with the detectors
decreases as $X$ increases. This can possibly be attributed to caching
effects (as the load increases, there is a larger chance that
simultaneous or sequential requests will be for the same URL), but it
should be the subject of further study.


\subsection{Comparison and comments about the tests}
\label{sec:tests-comparison}


The results of the NetPerf experiment are not surprising and show that
the impact of the detectors increases as the network load increases.
The impact of the detectors on the host could potentially be reduced
by improving the implementation of some of the detectors, particularly
the ESP-PORTSCAN detector. It keeps considerable state and maintains
some complex data structures, so it is likely to be one of the major
contributors to the impact that the detectors have.

Overall, the NetPerf results can be seen as indicative of detectors
that were designed and implemented with moderate success: their impact
is proportional to the amount of activity in the host, and their
impact is not excessive.

The http\_load results are visibly different from the NetPerf results.
Initially there is an extremely large difference between the ESP and
NOESP cases (over 300\% for $X=5$), but this difference decreases as
$X$ increases to the point where it is 65\% at $X=100$. While still
being a considerable difference, the reduction from the initial value
is dramatic.

The reduction in difference could be explained by a number of factors,
including caching effects on the web server. The http\_load makes
requests from a fixed-size list of URLs, and as the number of
simultaneous requests increases, the likelihood of requesting the same 
URL several times simultaneously or in close sequence increases. If
the web server is doing any caching of requests (so that it can serve
the same request multiple times without having to do all the
processing repeatedly), it could account for the reduction in CPU load 
for larger values of $X$.

More interesting for our purposes is the large effect that the
detectors have on the CPU load. This can be explained by the types of
detectors involved. The largest detector in the HTTP server is
ESP-BADURLS, a generic detector that does not directly cover any other
detectors, but that provides mechanisms for implementing several
others (see Figure~\ref{fig:by-implemented-by}). These mechanisms
consist mainly of a string-matching capability for detecting different
web-based attacks. What ESP-BADURLS does for every HTTP request is to
sequentially compare it against several strings using different
qualifications (such as anchoring the test string in different parts
of the request, checking the arguments of the URL, etc.). In this
respect, ESP-BADURLS is different from all the other detectors (which
check for a fixed condition) and could be expected \emph{a priori} to
have a larger impact on CPU utilization. Furthermore, the initial
implementation of ESP-BADURLS (used in these tests) uses a naive
approach, sequentially and blindly comparing the strings against each
request. By improving the implementation to use an efficient regular
expressions
engine~\citep[e.g.][]{houston01:spencer-regexp-library,hackerlab01:rx_posix-regexp-library}
or some other string-matching mechanism, it may be possible to reduce
the impact of the HTTP detectors considerably.

In conclusion, these tests point out a major consideration for the use
of internal sensors: they are heavily dependent on implementation
decisions, and a different implementation might make a significant
difference in performance and impact. Moreover, extreme care must be
taken in their implementation because when not implemented carefully,
they can have a severe impact on the performance of the monitored
component. \CB{These tests are not intended to provide measurements of
  the performance costs of embedded detectors in general, but only of
  our implementation, and to show the feasibility of doing
  low-overhead \id using the ESP architecture.}

This consideration is one of the reasons why internal sensors had not
been extensively studied before. Their implementation is complex, and
the possible consequences are severe. However, as will be described in
the next section, the payoff in detection capabilities can be
significant, and worth the work necessary to implement the sensors and
make them efficient.

%----------------------------------------------------------------------

\section{Detection testing}
\label{sec:detection-testing}

The purpose of the detection test was to determine the validity of the
second hypothesis (see Section~\ref{sec:thesis-stmt}): by using
internal sensors it is possible to detect new attacks.  Additionally,
we wanted to get an idea of the effort needed to improve the detection
capabilities of the detectors when necessary. To this end, a number of
previously unknown attacks were tested against a host instrumented
with ESP detectors.

\subsection{Test design and methodology}
\label{sec:detection-test-design}

As a source of information, we monitored the BugTraq mailing
list~\citep{bugtraq} for a period of slightly over one month, from
May~3, 2001 to June~8, 2001. During this period, 157 messages to the
mailing list were examined corresponding to reports of new
vulnerabilities, attacks and exploits against different systems. Of
these messages, 80 were determined to be applicable using the
criteria defined in Section~\ref{sec:cve-applicability}. Additionally, 
only messages that described specific attacks (and not only generic or 
vague vulnerabilities) were selected as applicable.

The 80 applicable attacks were tested against a host instrumented with 
the ESP implementation. We performed two types of testing depending
on the attack:

\begin{description}
\item[Real testing:] When an attack could be directly attempted
  against the OpenBSD system running ESP, we did so and recorded any
  responses from the existing detectors.
  
  For example, one of the attacks tested was against the
  \prog{crontab}~\citep{man:crontab} program. Because this program
  exists in OpenBSD, the exploit script could be run directly in our
  test system to determine whether the detectors would react to it.
  
\item[Simulated testing:] Sometimes an attack was not directly
  executable in our test platform---for example, because it used a
  program that does not exist in OpenBSD, or because it was specific
  to some other architecture. However, if the workings of the attack
  were clear enough, we did a ``simulated testing'' of the attack by
  studying its properties and determining whether any of the existing
  detectors would react to that attack if it were attempted against a
  system instrumented with ESP.
  
  For example, another one of the attacks examined was against the
  \prog{scoadmin} program~\citep{man:scoadmin} in the Unixware
  operating system. The affected program does not exist in OpenBSD,
  but the exploit was clear enough to show that it worked because
  \prog{scoadmin} followed a symbolic link in the \file{/tmp}
  directory. By this reasoning, we could determine that the attack
  would have been detected by the ESP-SYMLINK-OPEN detector.
\end{description}

After testing, each attack was classified in one or more of the
following categories (each category has a letter code associated with
it):

\begin{description}
\item[Detected (D):] The attack was detected by one or more of the
  existing detectors. In this case, we recorded the names of the
  detectors that reacted to the attack.

\item[Detected if successful (DS):] In some cases, the attack itself was
  not detected, but its effects would be if the attack were to be
  successful. In these cases, we also recorded which detectors would be 
  triggered by the successful attack.
  
  For example, the attack against \prog{cron} mentioned before was not
  immediately detected by any of the existing detectors. However, on
  success the attack would have created a root-owned set-UID copy of a
  shell, and this action would trigger the ESP-BADMODE-ROOT-FILE
  detector, so we classify this attack as ``detected if successful.''
  
\item[Detectable with modifications to existing detectors (DM):] Some
  attacks were not \linebreak detected by any of the existing
  detectors, but a reasonably small change to one of them would be
  sufficient to make the attack be detected. We considered as
  ``reasonably small'' changes that involved tuning some parameter of
  the detector, or slightly extending their functionality. In this
  case, we recorded the detector to which the changes would have to be
  made, and what those changes would be.

  For example, one of the entries reviewed was a web-based attack that 
  used a ``dot-dot'' (\file{../}) path to access files outside the
  normal web document directories, but with the variation that parts
  of the string were encoded in their hexadecimal representations to
  bypass checks at the web server (for example, \file{../} could be
  encoded as \file{.\%2e\%2f}, where 0x2E and 0x2F are the ASCII codes 
  for a dot and a slash respectively). This attack was not
  immediately detected by the existing ESP-URI-DOTDOT detector, but a
  small addition to make it ``unescape'' the strings before checking
  them would enable it to detect the attack.
  
\item[Detectable with creation of new detectors (DC):] Some attacks
  were not detected by the existing detectors, but they could be by
  creating a new one.  \CB{When the new detector would be a generic
    one---so that it would be able to detect multiple attacks and not
    only the one under testing---we considered this change as
    acceptable, because it provides for detection possibilities beyond
    the attack that prompted its creation.} In this case, we recorded
  the type of detector to create, its conditions for triggering, and a
  proposed name for it.
  
  For example, one of the attacks tested was a web-based buffer
  overflow, but using a long string in one of the HTTP headers
  included in a request (instead of being a long URL), so the existing
  ESP-LONGURL detector did not react to it. However, by implementing a
  similar detector called ESP-HTTP-HDR-OVERFLOW, which performs length
  checks on HTTP request headers, this attack (and possibly others)
  could be detected.
  
\item[Detectable if successful with modifications to existing
  detectors (SM):] {\ }\\This is similar to the DM category, but for the
  case in which the modifications to an existing detector would cause
  the attack to be detected only if successful.
  
  This is a possible category, but during the test no attacks were
  assigned to it.
  
\item[Detectable if successful with creation of new detectors (SC):]
  This is similar to the DC category, but for the case in which a new
  generic detector could be created to detect a successful attack.
  
  Only one entry was found in this category during the testing. It led
  to the creation of the ESP-PRIV-ESCALATION detector, which has the
  potential to detect many buffer overflow and race condition attacks
  in which a process acquires elevated privileges.
  
\item[Not detectable (ND):] An attack was considered in this category
  when the only way to detect it would have been to create a new
  specific detector for it. \CB{Creating a specific detector does not
    provide any future benefits (possibility for detection of other
    attacks other than the current one), so it was not considered as
    an acceptable change for our purposes.}
  
  For example, one entry reviewed consisted of an attack against the
  \prog{xfs} (X font server) in certain versions of XFree86, which
  would crash when fed a long random string, causing a
  denial-of-service attack. For detecting this attack, it would be
  necessary to implement a new specific detector in the \prog{xfs}
  code. Therefore we consider it as not detectable.

\end{description}

An entry can belong to any of these categories and can also belong to
both DS and DC (DSDC) or DS and DM (DSDM). This occurs when an attack
is detectable if successful but it could also be detected by either
creating or modifying a detector.

The testing was divided in four batches of 20 attacks. After every
batch, all the changes recorded for detectors in categories DM, DC and
SC were applied, so after each batch all the entries in those
categories would belong to category D.


\subsection{Results from the detection test}
\label{sec:results-detection-test}

In total, 157 attacks were examined, of which 80 were applicable. Of
these, 47 were done with real testing, and 33 with simulated testing.

The number of attacks in each category for each one of the batches and
for the whole test are shown in Table~\ref{tab:attacks-numbers}.
\input{tables/attacks-numbers} The total categories (TD, TDM and ND)
are displayed also in Figure~\ref{fig:attack-batches}.
\input{figures/fig_attack_batches} In this chart, we can see that the
number of attacks detected was consistently over 30\% in each of the
batches. When counting the attacks that were detected after making
modifications to the detectors, the number was consistently over 50\%.

Keeping in mind that each batch incorporates the changes made after
the previous batch, it is also of interest to analyze the total
results at the beginning and at the end. This is, if all the 80
attacks had been applied to the original detectors, how many would
have been detected? By comparing this with the number of attacks
detected at the end of the test (after all the changes were made), we
can observe the impact that the changes had in the detection
capabilities.  Figure~\ref{fig:detectors-original-final} shows these
numbers graphically. We can see that even without any modifications,
the original ESP detectors would have been able to detect 35\% of the
new attacks (41.2\% if we count the ones in group DS). After making
the changes, the detection rate went up to 62.5\% (71.2\% with DS).
\input{figures/detectors-original-final}

\subsubsection{Original detection capabilities}

Of the 80 attacks exercised during this test, 33 would have been
detected by the ESP detectors as originally implemented.
Table~\ref{tab:original-det-detectors} shows the distribution of the
detectors involved. Table~\ref{tab:original-det-krsul} shows the
distribution of the Krsul categories assigned to each attack (see
Appendix~\ref{chap:app_krsul}).
\input{tables/original-det-detectors}
\input{tables/original-det-krsul}

Looking at these tables it is possible to see some relationships. For
example, the most successful detector is ESP-SYMLINK-OPEN, which
triggers when a symbolic link is accessed in a temporary directory.
Coupled with the high occurrence of attacks in category 2-12-2-1
(which refers to programs assuming that a file path refers to a valid
temporary file), it is an indicator of the high occurrence of the
so-called ``bad symlink'' attacks, in which a program can be tricked
into following a symbolic link placed by the attacker to modify or
read system files.

Also of interest is the high occurrence of attacks in category 2-2-1-1
(programs assuming that user input is at most of a certain length)
that corresponds in general to buffer overflow attacks. These attacks
are detected mainly by the ESP-ARGS-LEN, ESP-ENV-LEN, ESP-LONGURL and
ESP-FTP-CMD-OVERFLOW detectors.

\CB{We implemented detectors for 130 out of 815 entries in the CVE
  database (see Sections~\ref{sec:sources}
  and~\ref{sec:detectors-implemented}), corresponding to 15.9\% of the
  entries, both applicable and non-ap\-pli\-ca\-ble. Those detectors were
  able to detect 38 of the total 157 entries (both applicable and
  non-applicable) examined in the detection test, corresponding to a
  24.2\% detection rate. These numbers are encouraging because we can
  expect that by implementing detectors for more CVE entries, a larger
  number of generic detectors could be designed and implemented,
  providing even larger detection capabilities for new attacks.}

\subsubsection{Effects of changes on detection capabilities}
\label{sec:det-effects-changes}

The changes done to the detectors during this test are listed in
Table~\ref{tab:det-changes} according to the detector they affected.
\input{tables/det-changes}
In total, 65 executable statements were added or modified in these
changes, and the changes caused an increase of 30\% in the detection
capabilities of the detectors.

Tables~\ref{tab:final-det-detectors} and~\ref{tab:final-det-krsul}
list the distribution of detectors and Krsul classifications for the
detection capabilities of the final detectors (after the
modifications). 

\input{tables/final-det-detectors}
\input{tables/final-det-krsul}

Figure~\ref{fig:detexp-krsul-original-final} shows the percentage of
attacks in each Krsul category that occurred in the test, and the
percentage of those attacks that were detected by the original and the
final detectors. Also, to make it easier to see the differences
between the original and final detection capabilities by type of
vulnerability, Figure~\ref{fig:detexp-krsul-original-final-plot} shows
the percentage of attacks detected by the original and final
detectors, plotted as $x,y$ coordinates. In this plot, those points
farther above the identity line represent the categories with the most
improvement as a result of the changes.
\input{figures/fig_detexp_krsul_original_final}
\input{figures/fig_detexp_krsul_original_final_plot}

One notable change is the increase in the detection of attacks from
categories 2-12-2-2 and 2-11-1-1 (both of which correspond to programs
assuming that a path name given by the user is in valid space for the
application).  This can be attributed to the improvements to the
ESP-URI-DOTDOT detector, as well as the creation of
ESP-FTP-CMD-DOTDOT.

We can also see that there was a 100\% detection rate for category
2-12-2-1 (corresponding to ``bad symlink'' attacks), which shows the
effectiveness of the corresponding detector (ESP-SYMLINK-OPEN).

In a similar fashion, Figure~\ref{fig:detexp-detectors-original-final}
shows the percentage of attacks detected by each detector before and
after the changes, and
Figure~\ref{fig:detexp-detectors-original-final-plot} plots the
``before'' and ``after'' percentages as $x,y$ coordinates.
\input{figures/fig_detexp_detectors_original_final}
\input{figures/fig_detexp_detectors_original_final_plot} We can see
that ESP-URI-DOTDOT was the detector with the largest improvement.
This is mostly the result of the changes in the way encoded characters
are examined and to the large occurrence of attacks of type 2-12-2-2
and 2-11-1-1 (which this detector corresponds to). We can also see a
considerable improvement in ESP-BADURLS (because of the addition of
new patterns), and of some of the generic detectors created as part of
the changes.

\subsection{Comments about the detection test}
\label{sec:comments-det-test}

%Figure~\ref{fig:krsul-esp-test} shows the distribution of Krsul
%categories (see Appendix~\ref{chap:app_krsul}) for the original ESP
%detectors and for the attacks that were selected as applicable during
%this test.
%\input{figures/fig_krsul-esp-test}

To evaluate the similarity of the attacks used in the detection test
to the types of attacks for which the ESP detectors were implemented,
we plotted each category using its occurrence in the ESP detectors and
in the test set as $x,y$ coordinates, as shown in
Figure~\ref{fig:krsul_counts_esp-detexp}. We can see that most points
are close to the identity line (shown for reference), indicating that
the two distributions are indeed similar. The Pearson correlation
($r$) for these points is 0.673, confirming the strong linear
correlation. A linear regression of the points results in the
following formula: \[ y = 1.051x + 0.0694, \] which is close to the
identity line, providing a third confirmation of the similarity
between the two distributions.
\input{figures/fig_krsul_counts_esp-detexp}

\CB{This similarity suggests the validity of using random drawing from
  the CVE as a guide for the implementation of the ESP detectors,
  because it resulted in detectors for a population of attacks similar
  to those encountered in ``the real world.''}

Particularly similar (close to the identity line) are categories
2-2-1-1, 2-5-1-1, 2-3-2-1 (all three of which correspond to buffer
overflow attacks), 2-2-1-3 and 2-2-1-4 (corresponding to programs
failing to check the form of user-provided input).  Others, such as
2-12-2-1 (mostly symlink-based attacks) and 2-12-2-2 (mostly
``dot-dot'' attacks) have a larger representation in the test set than
in the detectors implemented, but correspond to some of the most
effective generic detectors that were implemented (see
Tables~\ref{tab:original-det-detectors}
and~\ref{tab:final-det-detectors}).

% FIX THE FOLLOWING

Assuming that the set of applicable attacks in the detection test is
\CB{representative of the new attacks that continuously appear in the
  real world, we can make some predictions about the detection
  capabilities of the ESP detectors (this assumption should be
  evaluated in future work by sampling sets of attacks that appeared
  during different periods of time).} As a first approach to these
predictive capabilities, we can use the standard formula for computing
a 95\% confidence interval on a proportion $p$:
\[ \text{95\% Confidence interval} = p \pm \left(1.96\sqrt{\frac{p(1-p)}{N}} 
    + \frac{0.5}{N}\right).
\label{frm:ci}\]
The 95\% confidence intervals for the percentages of detection before
and after the changes made to the detectors are shown in
Table~\ref{tab:detexp-confidence-intervals}. Further possibilities for
prediction of detection capabilities are described in
Section~\ref{sec:future-work}.
\input{tables/detexp-confidence-intervals}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "~/Thesis/Dissertation/thesis"
%%% End: 
