% Table showing a classification of IDSs according to the
 % architectures defined in Chapter 2.

\begin{sidewaystable}
  \makeatletter\renewcommand{\baselinestretch}{1}\reset@font\makeatother
  \begin{center}
    \caption[Classification of some existing \idss by their data
    collection mechanisms and data analysis structure]{Classification
      of some existing \idss according to the architectures they use
      for their data collection mechanisms and their data analysis
      structure, as described in Sections~\ref{sec:direct-indirect},
      \ref{sec:host-network}, \ref{sec:external-internal}
      and~\ref{sec:analysis-architectures}. Note that the assigned
      percentages for data collection mechanisms do not add to 100\%
      because some \idss use more than one type of data collection
      mechanism.}
    \label{tab:classification_ids}
    \newcommand{\EC}{\multicolumn{1}{c}{}}
    \newcommand{\tabcell}[2][l]{\begin{tabular}{#1}#2\end{tabular}}
    \renewcommand{\tabularxcolumn}[1]{>{\raggedright\arraybackslash}m{#1}}
    {\small\begin{tabularx}{\linewidth}{|c|c|l|X|X|}\cline{4-5}
      \EC & \EC & & \multicolumn{2}{c|}{\tabtitnocent{Data analysis structure}}\\\cline{4-5}
      \EC & \EC & &
          \multicolumn{1}{c}{\tabtitnocent{Centralized (--pct(das,Centralized)\%)}} &
          \multicolumn{1}{c|}{\tabtitnocent{Distributed (--pct(das,Distributed)\%)}} \\\cline{1-5}
      \multirow{11}{*}[-5em]{\begin{sideways}\textbf{Data collection mechanisms}\end{sideways}} &
        \multirow{7}{*}[-4em]{\begin{sideways}\textbf{Indirect (--pct(dcm,Indirect/NetBased,Indirect/HostBased)\%)}\end{sideways}} &
          \tabcell{Network-based\\(--pct(dcm,Indirect/NetBased)\%)} &
          % Centralized/Indirect/NetworkBased
          --idss(Centralized,any,Indirect/NetBased,any)
          &
          % Distributed/Indirect/NetworkBased
          --idss(Distributed,any,Indirect/NetBased,any)
           \\\cline{3-5}
      & & \tabcell{Host-based\\(--pct(dcm,Indirect/HostBased)\%)} &
           % Centralized/Indirect/HostBased
           --idss(Centralized,any,Indirect/HostBased,any)
      &
           % Distributed/Indirect/HostBased
           --idss(Distributed,any,Indirect/HostBased,any)
           \\\cline{2-5}
      & \multirow{2}{*}[0.5em]{\begin{sideways}\tabcell[c]{\textbf{Direct}\\\textbf{(--pct(dcm,Direct/External,Direct/Internal)\%)}}\end{sideways}} &
          \tabcell{Host-based\\external (--pct(dcm,Direct/External)\%)} &
          % Centralized/Direct/External
          --idss(Centralized,any,Direct/External,any)
          &
          % Distributed/Direct/External
          --idss(Distributed,any,Direct/External,any)
           \\\cline{3-5}
      & & \tabcell{Host-based\\internal (--pct(dcm,Direct/Internal)\%)} &
      % Centralized/Direct/Internal
      --idss(Centralized,any,Direct/Internal,any)
      &
      % Distributed/Direct/Internal
      --idss(Distributed,any,Direct/Internal,any)
      \\\hline
    \end{tabularx}}
  \end{center}
\end{sidewaystable}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "~/Thesis/Dissertation/thesis"
%%% End: 
